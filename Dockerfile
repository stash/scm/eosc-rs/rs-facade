FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9
WORKDIR /app

COPY requirements.txt /app/

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./facade_app /app/facade_app
COPY ./tests /app/facade_app

ENV PORT=8001
ENV MODULE_NAME=facade_app.main