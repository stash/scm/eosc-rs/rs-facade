"""Aiohttp client"""
import datetime
import json
import logging

import aiohttp

from facade_app.settings import settings_default


class DateTimeEncoder(json.JSONEncoder):
    """DateTime encoder to json"""

    def default(self, o):
        """Default encoding function

        Args:
            o (Union[datetime.date, datetime.datetime]): Object to encode

        Returns:
            str: Date in ISO 8601 format YYYY-MM-DD or
                datetime in format: YYYY-MM-DDTHH:MM:SS
        """
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()


def json_dumps_datetime(*args, **kwargs):
    """Json serializer with DateTimeEncoder"""
    kwargs["cls"] = DateTimeEncoder
    return json.dumps(*args, **kwargs)


class ApiClient:  # pylint: disable=unused-argument
    """Aiohttp session wrapper"""

    def __init__(self):
        self.session: aiohttp.ClientSession = None

    async def create_session(self):
        """Create aiohttp session"""

        async def on_request_start(session, context, params):
            logging.getLogger("aiohttp.client").debug(f"Starting request <{params}>")

        async def on_request_chunk_sent(session, context, params):
            logging.getLogger("aiohttp.client").debug(f"Request sent <{params}>")

        async def on_request_end(session, context, params):
            logging.getLogger("aiohttp.client").debug(f"End request <{params}>")

        trace_config = aiohttp.TraceConfig()
        if settings_default.ENABLE_AIOHTTP_TRACING == "yes":
            trace_config.on_request_start.append(on_request_start)
            trace_config.on_request_chunk_sent.append(on_request_chunk_sent)
            trace_config.on_request_end.append(on_request_end)

        self.session = aiohttp.ClientSession(
            trace_configs=[trace_config], json_serialize=json_dumps_datetime
        )

    async def close_session(self):
        """Close aiohttp session"""
        if self.session is not None:
            await self.session.close()


api_client = ApiClient()
