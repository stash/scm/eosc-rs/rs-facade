"""A module with basic utils."""
from pathlib import Path
from typing import Union

import requests


def get_project_root() -> Path:
    """The function returns the path to the root folder of the project.

    Returns:
        Path: Path to root of project
    """
    return Path(__file__).parent.parent


def parse_error_response_details(response: requests.Response) -> Union[str, dict]:
    """The function that parses error messages received in response.

    Args:
        response (requests.Response): Response message to parse

    Returns:
        Union[str,dict]: If the response is json then a dict will be returned
            otherwise the message text will be str.
    """
    if response.headers.get("content-type") == "application/json":
        return response.json()
    else:
        return response.text
