"""A module that generates recommendations fo resources."""
import asyncio
import logging
from typing import Dict, List, Tuple

import httpx
import pandas as pd
from fastapi import APIRouter
from fastapi.encoders import jsonable_encoder
from pydantic import ValidationError, parse_obj_as

from facade_app.api.schemas.recommendation_context import RecommendationContext
from facade_app.api.schemas.recommendation_context_resources import (
    RecommendationContextResources,
)
from facade_app.api.schemas.recommendation_context_service import (
    RecommendationContextServices,
)
from facade_app.api.schemas.recommendation_set import RecommendationSet
from facade_app.api.schemas.recommendation_set_with_score import (
    RecommendationSetWithScore,
)
from facade_app.exceptions import (
    EmptyCandidateList,
    RemoteApiUnavailableError,
    RemoteApiValidationError,
)
from facade_app.jms_connector import JmsConnector
from facade_app.settings import Settings, settings_default
from facade_app.structures.types import AggregationType, PanelId

api_router = APIRouter()
logger = logging.getLogger(__name__)

jms_connector = JmsConnector()


class EngineRecommendation:
    """Engine class that generates recommendations."""

    def __init__(self, settings: Settings = settings_default):
        self.settings = settings

    async def get_recommendation(
        self,
        url: str,
        request: RecommendationContext,
        panel_id: PanelId,
        engine_version: str,
        service_name: str,
        data: dict[str, List[RecommendationSetWithScore]],
        status_data: dict[str],
        sources: List[Dict],
        recommender_systems: List[str],
    ):
        """Get recommendation from remote recommender

        Args:
            url (str): URL of remote recommender
            request (RecommendationContext): Recommendation context
            panel_id (PanelId): Panel id
            engine_version (str): Engine version
            service_name (str): Remote service name
            data (dict[str, List[RecommendationSetWithScore]]):  Object to store responses
                from remote recommender
            status_data (dict[str]): Object to store status
                of remote recommender
            sources (List[Dict]): Object to store source
                of recommendation
            recommender_systems (List[str]): Object to store recommender systems name

        """
        request.panel_id = panel_id

        if engine_version is not None and engine_version.endswith("sort"):
            if service_name == "Marketplace_rs":
                engine_version = "NCFRanking"
            if len(request.candidates) == 0:
                logger.warning(f"Candidate list for {panel_id} to sort is empty.")
                status_data[panel_id] = EmptyCandidateList(
                    400,
                    f"Candidate list for {panel_id} to sort is empty.",
                )
                return None
        request.engine_version = engine_version

        request_data = jsonable_encoder(request, exclude_none=True)
        async with httpx.AsyncClient() as client:
            try:
                response = await client.post(
                    url, json=request_data, timeout=self.settings.EXTERNAL_API_TIMEOUT
                )
                if response.status_code == 200:
                    if service_name not in recommender_systems:
                        recommender_systems.append(service_name)
                    response_json = response.json()
                    engine_version = (
                        response_json["engine_version"]
                        if "engine_version" in response_json
                        else ""
                    )
                    sources.append(
                        {
                            "panel_id": panel_id,
                            "recommender_system": service_name,
                            "recommendations": response_json["recommendations"],
                            "engine_version": engine_version,
                        }
                    )
                    data[panel_id] = parse_obj_as(
                        RecommendationSetWithScore, response_json
                    )
                    status_data[panel_id] = "ok"
                else:
                    error_text = response.text.replace("\n", "\\n").replace("\r", "\\r")
                    logger.error(f"Error connecting to {service_name}: {error_text}")
                    status_data[panel_id] = RemoteApiUnavailableError(
                        service_name,
                        url,
                        response.status_code,
                        response.text,
                    )
            except ValidationError as exc:
                logger.error(
                    f"Error during validation response from {service_name} \
response {response.text}; errors: {exc.errors()}"
                )
                status_data[panel_id] = RemoteApiValidationError(
                    service_name,
                    url,
                    400,
                    response.text,
                )
            except httpx.TimeoutException as exc:
                logger.error(f"Error connecting to {service_name}: Timeout {exc}")
                status_data[panel_id] = RemoteApiUnavailableError(
                    service_name,
                    url,
                    408,
                    f"Timeout {exc}",
                )
            except (httpx.ConnectError, httpx.RemoteProtocolError) as exc:
                logger.error(f"Error connecting to {service_name}: {exc}")
                status_data[panel_id] = RemoteApiUnavailableError(
                    service_name, url, 408, str(exc)
                )

    def aggregate_to_one_panel_by_sort(
        self,
        data: dict[str, List[RecommendationSetWithScore]],
        panel_id: PanelId,
        take: int,
    ) -> RecommendationSet:
        """Function that aggregate recommendations from multiple recommender
            by sorting by score

        Args:
            data (dict[str, List[RecommendationSetWithScore]]): Object with recommendations
            panel_id (PanelId): Panel id
            take (int): Number of resources to return

        Returns:
            RecommendationSet: A collection of recommended resources.
        """
        recommendation_set_with_score = RecommendationSetWithScore(
            panel_id=panel_id,
            recommendations=[],
            explanations=[],
            explanations_short=[],
            scores=[],
        )
        engine_versions = []
        for _, value in data.items():
            recommendation_set_with_score.add_other(value)
            engine_versions.append(value.engine_version)
        recommendation_set_with_score.sort(take=take)

        recommendation_set = RecommendationSet.from_other(recommendation_set_with_score)
        recommendation_set.engine_versions = list(set(engine_versions))
        return recommendation_set

    def aggregate_to_one_panel_by_top(
        self,
        data: dict[str, List[RecommendationSetWithScore]],
        panel_id: PanelId,
        take: int,
    ) -> RecommendationSet:
        """Function that aggregate lists whit RecommendationSetWithScore
        by taking first row from each list.

        Args:
            data (dict[str, List[RecommendationSetWithScore]]): Object with recommendations
            panel_id (PanelId): Panel id
            take (int): Number of resources to return

        Returns:
            RecommendationSet: A collection of recommended resources.
        """
        recommendation_set_with_score = RecommendationSetWithScore(
            panel_id=panel_id,
            recommendations=[],
            explanations=[],
            explanations_short=[],
            scores=[],
        )
        engine_versions = []
        recommendations_dfs = []
        for source_panel, value in data.items():
            df_other = value.to_pandas()
            df_other["source"] = source_panel
            recommendations_dfs.append(df_other)
            engine_versions.append(value.engine_version)

        recommendations_df = pd.concat(recommendations_dfs)
        take_from_one = take // len(recommendations_dfs)
        df_grouped = recommendations_df.sort_values("score", ascending=False).groupby(
            "source"
        )
        recommendations_df = df_grouped.head(take_from_one)
        if len(recommendations_df) < take:
            recommendations_df = pd.concat(
                [recommendations_df, df_grouped.nth(take_from_one)]
            )
        recommendation_set_with_score.from_pandas(recommendations_df.head(take))
        recommendation_set = RecommendationSet.from_other(recommendation_set_with_score)
        recommendation_set.engine_versions = list(set(engine_versions))
        return recommendation_set

    def aggregate_simple(
        self, data: dict[str, List[RecommendationSetWithScore]], panel_id: PanelId
    ) -> RecommendationSet:
        """Simple aggregation by concatenate recommendation.

        Args:
            data (dict[str, List[RecommendationSetWithScore]]): Object with recommendations
            panel_id (PanelId): Panel id

        Returns:
            RecommendationSet: A collection of recommended resources.
        """
        recommendation_set = RecommendationSet.from_other(data[panel_id])
        recommendation_set.engine_versions = [data[panel_id].engine_version]
        return recommendation_set

    async def recommendations(
        self,
        request: RecommendationContext,
    ) -> Tuple[RecommendationSet, List[Dict], List[str]]:
        """Function that generate recommendations

        Args:
            request (RecommendationContext): Recommendation context

        Returns:
            Tuple[RecommendationSet, List[Dict], List[str]]:
                A collection of recommended resources.
                List of recommendation sources.
                List of  recommender systems.
        """
        panel_id = request.panel_id
        engine_version = request.engine_version
        data: dict = {}
        status_data: dict = {}
        sources: List[Dict] = []
        recommender_systems: List[str] = []
        if panel_id in [
            PanelId.SERVICES,
            PanelId.SERVICES_V1,
            PanelId.SERVICES_V2,
            PanelId.DATA_SOURCES,
        ]:
            if panel_id == PanelId.SERVICES:
                panel_id = PanelId.SERVICES_V1
            candidates: List[str] = []
            if panel_id == PanelId.DATA_SOURCES:
                candidates = request.candidates.datasource
            else:
                candidates = request.candidates.service
            await asyncio.gather(
                self.get_recommendation(
                    self.settings.CYFRONET_RS_URL + "/recommendations",
                    RecommendationContextServices.from_recommendation_context(
                        request, candidates
                    ),
                    panel_id,
                    engine_version,
                    "Marketplace_rs",
                    data,
                    status_data,
                    sources,
                    recommender_systems,
                )
            )
            if len(data) == 0:
                raise list(status_data.values())[0]
            recommendation_set = self.aggregate_simple(data, panel_id)
        elif panel_id == PanelId.ALL:
            await asyncio.gather(
                self.get_recommendation(
                    self.settings.CYFRONET_RS_URL + "/recommendations",
                    RecommendationContextServices.from_recommendation_context(
                        request, request.candidates.service
                    ),
                    PanelId.SERVICES_V1,
                    request.engine_version,
                    "Marketplace_rs",
                    data,
                    status_data,
                    sources,
                    recommender_systems,
                ),
                self.get_recommendation(
                    self.settings.CYFRONET_RS_URL + "/recommendations",
                    RecommendationContextServices.from_recommendation_context(
                        request, request.candidates.datasource
                    ),
                    PanelId.DATA_SOURCES,
                    request.engine_version,
                    "Marketplace_rs",
                    data,
                    status_data,
                    sources,
                    recommender_systems,
                ),
                self.get_recommendation(
                    self.settings.ONLINE_ENGINE_URL + "/recommendations",
                    RecommendationContextResources.from_recommendation_context(
                        request, candidates=request.candidates.dataset
                    ),
                    PanelId.DATASETS,
                    request.engine_version,
                    "Online_engine",
                    data,
                    status_data,
                    sources,
                    recommender_systems,
                ),
                self.get_recommendation(
                    self.settings.ONLINE_ENGINE_URL + "/recommendations",
                    RecommendationContextResources.from_recommendation_context(
                        request, candidates=request.candidates.publication
                    ),
                    PanelId.PUBLICATIONS,
                    request.engine_version,
                    "Online_engine",
                    data,
                    status_data,
                    sources,
                    recommender_systems,
                ),
                self.get_recommendation(
                    self.settings.ONLINE_ENGINE_URL + "/recommendations",
                    RecommendationContextResources.from_recommendation_context(
                        request, candidates=request.candidates.software
                    ),
                    PanelId.SOFTWARE,
                    request.engine_version,
                    "Online_engine",
                    data,
                    status_data,
                    sources,
                    recommender_systems,
                ),
                self.get_recommendation(
                    self.settings.ONLINE_ENGINE_URL + "/recommendations",
                    RecommendationContextResources.from_recommendation_context(
                        request, candidates=request.candidates.training
                    ),
                    PanelId.TRAININGS,
                    request.engine_version,
                    "Online_engine",
                    data,
                    status_data,
                    sources,
                    recommender_systems,
                ),
                self.get_recommendation(
                    self.settings.ONLINE_ENGINE_URL + "/recommendations",
                    RecommendationContextResources.from_recommendation_context(
                        request, candidates=request.candidates.other
                    ),
                    PanelId.OTHERRESEARCHPRODUCT,
                    request.engine_version,
                    "Online_engine",
                    data,
                    status_data,
                    sources,
                    recommender_systems,
                ),
            )
            if len(data) == 0:
                raise sorted(list(status_data.values()), key=lambda x: x._order)[0]
            if engine_version is not None and engine_version.endswith("sort"):
                recommendation_set = self.aggregate_to_one_panel_by_sort(
                    data, panel_id, -1
                )
            elif self.settings.AGGREGATION_TYPE == AggregationType.SORT:
                recommendation_set = self.aggregate_to_one_panel_by_sort(
                    data, panel_id, self.settings.NUMBER_OF_RECOMMENDATION
                )
            else:
                recommendation_set = self.aggregate_to_one_panel_by_top(
                    data, panel_id, self.settings.NUMBER_OF_RECOMMENDATION
                )

        else:
            data: dict = {}
            candidates: List[str] = []
            if panel_id == PanelId.DATASETS:
                candidates = request.candidates.dataset
            elif panel_id == PanelId.PUBLICATIONS:
                candidates = request.candidates.publication
            elif panel_id == PanelId.OTHERRESEARCHPRODUCT:
                candidates = request.candidates.other
            elif panel_id == PanelId.SOFTWARE:
                candidates = request.candidates.software
            elif panel_id == PanelId.TRAININGS:
                candidates = request.candidates.training
            await asyncio.gather(
                self.get_recommendation(
                    self.settings.ONLINE_ENGINE_URL + "/recommendations",
                    RecommendationContextResources.from_recommendation_context(
                        request, candidates=candidates
                    ),
                    panel_id,
                    engine_version,
                    "Online_engine",
                    data,
                    status_data,
                    sources,
                    recommender_systems,
                )
            )
            if len(data) == 0:
                raise list(status_data.values())[0]
            recommendation_set = self.aggregate_simple(data, panel_id)

        return recommendation_set, sources, recommender_systems


engine_recommendation = EngineRecommendation()
