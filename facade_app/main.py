"""Main module"""
import json
import logging
import sys

from fastapi import FastAPI, Request
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse

from facade_app.api.endpoints import (
    diagnostic,
    project_completion,
    recommendations,
    similar_resources,
    similar_services,
    statistics,
)
from facade_app.api.schemas.problem_details import ProblemDetails
from facade_app.api_client import api_client
from facade_app.exceptions import BaseApiException, RemoteApiError
from facade_app.settings import settings_default, tags_metadata, API_VERSION


logging.basicConfig(
    stream=sys.stdout,
    level=logging.getLevelName(settings_default.LOG_LEVEL),
    format="[%(asctime)s] [%(levelname)s] %(name)s: %(message)s",
    datefmt="%Y-%m-%d:%H:%M:%S",
)

logger = logging.getLogger(__name__)
logging.getLogger("uvicorn.access").setLevel(
    logging.getLevelName(settings_default.LOG_LEVEL)
)
app = FastAPI(
    title=settings_default.PROJECT_NAME,
    openapi_url=f"{settings_default.API_STR}/openapi.json",
    version=API_VERSION,
    openapi_tags=tags_metadata,
)
app.include_router(recommendations.api_router)
app.include_router(project_completion.api_router)
app.include_router(similar_services.api_router)
app.include_router(similar_resources.api_router)
app.include_router(diagnostic.api_router)
app.include_router(statistics.api_router)


@app.on_event("startup")
async def on_startup():
    """Event that is running before application start.
    Its logs: version, and swagger and set database connectors to app.state
    """
    logger.info(f"API version {API_VERSION}")
    if settings_default.SAVE_SWAGGER != "no":
        openapi_data = app.openapi()
        with open("docs/swagger.json", "w+", encoding="utf8") as file:
            json.dump(openapi_data, file, indent=3)
    await api_client.create_session()


@app.on_event("shutdown")
async def on_shutdown():
    """Event that is running before shutdown application.
    Its close connection pools and session.
    """
    await api_client.close_session()


@app.exception_handler(RemoteApiError)
async def http_exception_handler(request: Request, exc: RemoteApiError) -> JSONResponse:
    """Exception handler for RemoteApiError

    Args:
        request (Request): Request
        exc (RemoteApiError): Error

    Returns:
        JSONResponse: Json response with error details.
    """
    problem_details = ProblemDetails(
        detail=exc.detail,
        status=exc.status_code,
        instance=str(request.url),
        component=exc.component,
    )
    if exc.status_code == 404:
        problem_details.title = "NotFound"
    elif exc.status_code == 400:
        problem_details.title = "BadRequest"
    elif exc.status_code == 500:
        problem_details.title = "UnspecifiedProblem"
    return JSONResponse(
        content=jsonable_encoder(problem_details.dict(), exclude_none=True),
        status_code=exc.status_code,
        media_type="application/problem+json",
    )


@app.exception_handler(BaseApiException)
async def base_exception_handler(
    request: Request, exc: BaseApiException
) -> JSONResponse:
    """Exception handler for BaseApiException

    Args:
        request (Request): Request
        exc (BaseApiException): Error

    Returns:
        JSONResponse: Json response with error details.
    """
    logger.error(
        f"{exc.__class__.__name__} url: {request.url} status: {exc.status_code} detail {exc.detail}"
    )
    problem_details = ProblemDetails(
        title=exc.__class__.__name__,
        detail=exc.detail,
        status=exc.status_code,
        instance=str(request.url),
    )
    return JSONResponse(
        content=jsonable_encoder(problem_details.dict()),
        status_code=exc.status_code,
        media_type="application/problem+json",
    )
