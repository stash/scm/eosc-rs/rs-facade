"""A module with all enum types."""
from enum import Enum


class PanelId(str, Enum):
    """Panel Id."""

    ALL = "all"
    SERVICES = "services"
    SERVICES_V1 = "v1"  # services
    SERVICES_V2 = "v2"  # services
    DATASETS = "datasets"
    PUBLICATIONS = "publications"
    SOFTWARE = "software"
    TRAININGS = "trainings"
    OTHERRESEARCHPRODUCT = "other_research_product"
    DATA_SOURCES = "data-sources"
    SIMILAR_SERVICES = "similar_services"


class AggregationType(str, Enum):
    """Type of aggregation of recommendations."""

    TOP = "top"
    SORT = "sort"


class ResourceType(str, Enum):
    """Resource type."""

    DATASETS = "datasets"
    PUBLICATIONS = "publications"
    SOFTWARE = "software"
    TRAININGS = "trainings"
    OTHERRESEARCHPRODUCT = "other_research_product"


class ClientId(str, Enum):
    """Client id."""

    MARKETPLACE = "marketplace"
    SEARCH_SERVICE = "search_service"
    USER_DASHBOARD = "user_dashboard"
    UNDEFINED = "undefined"
