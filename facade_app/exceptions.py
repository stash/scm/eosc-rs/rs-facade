"""A module with exceptions."""
import functools
from aiohttp import ClientConnectorError, ClientError
from fastapi import HTTPException


class RemoteApiError(Exception):
    """Base class for remote API errors"""

    _order = 90

    def __init__(self, component: str, url: str, status_code: int, detail: str):
        self.component = component
        self.url = url
        self.status_code = status_code
        self.detail = detail

    def __str__(self):
        return f"""{self.__class__.__name__} component: {self.component}
        url: {self.url} status: {self.status_code} detail: {self.detail}"""


class RemoteApiUnavailableError(RemoteApiError):
    """Remote Api Unavailable Error, e.g. unexpected error code"""

    _order = 50


class RemoteApiValidationError(RemoteApiError):
    """Remote Api Validation Error, e.g. unexpected error during validation
    response from remote api"""

    _order = 40


class BaseApiException(HTTPException):
    """Base exception"""

    _order = 90


class EmptyCandidateList(BaseApiException):
    """Candidates to sort is empty"""

    _order = 100


def catch_client_error(component: str):
    """Catch aiohttp errors and raise RemoteApiUnavailableError"""

    def decorator(func):
        @functools.wraps(func)
        async def wrapper(*args, **kwargs):
            try:
                return await func(*args, **kwargs)
            except ClientConnectorError as exc:
                raise RemoteApiUnavailableError(
                    component, exc.host, 503, str(exc)
                ) from exc
            except ClientError as exc:
                raise RemoteApiUnavailableError(component, "", 503, str(exc)) from exc
            except TimeoutError as exc:
                raise RemoteApiUnavailableError(
                    component, "", 503, "Remote API Timeout"
                ) from exc

        return wrapper

    return decorator
