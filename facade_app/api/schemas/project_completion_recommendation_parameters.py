# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Definition project completion recommendation parameters."""

from pydantic import BaseModel, Field


class ProjectCompletionRecommendationParameters(BaseModel):
    """Project context for recommendations."""

    project_id: int = Field(title="Project Id")
    num: int = Field(title="Num")
