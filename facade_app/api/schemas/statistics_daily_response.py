# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

""" Models for response for daily statistics."""
from pydantic import BaseModel, Field


class StatisticsDailyResponse(BaseModel):
    """Daily statistics"""

    date: str = Field(title="Date")
    recommendations: int = Field(title="Recommendations")
