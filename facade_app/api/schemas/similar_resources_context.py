# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Models for the similar resources endpoint."""
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field

from facade_app.structures.types import ClientId, ResourceType


class SimilarResourcesContext(BaseModel):
    """User and resource context for recommendations."""

    user_id: Optional[int] = Field(
        title="User ID",
        description="The unique marketplace identifier of the logged user.",
        default=None,
    )
    unique_id: str = Field(
        title="Not logged user ID",
        description="The unique identifier of the not logged user.",
    )
    aai_uid: Optional[str] = Field(
        title="AAI user ID",
        description="The unique AAI identifier of the logged user.",
        default=None,
    )
    timestamp: datetime = Field(
        title="Timestamp",
        description="The exact time of the recommendation request sending "
        "in iso8601 format.",
    )
    page_id: str = Field(
        title="Page ID",
        description="The unique identifier of the page with recommendation panel.",
    )
    panel_id: str = Field(
        title="Version of the panel",
        description="The unique identifiers of the set of recommender panels on the page.",
    )
    resource_id: str = Field(
        title="Resource unique identifier",
    )
    resource_type: ResourceType = Field(
        title="Resource type",
    )

    client_id: Optional[ClientId] = Field(title="Client ID", default=None)
    engine_version: Optional[str] = Field(title="Engine version", default=None)
