# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Models for the most recommended along your services endpoint."""
from typing import Optional

from pydantic import BaseModel, Field


class StatisticsMostRecommendedAlongYourServices(BaseModel):
    """Provider and service context"""

    provider_id: str = Field(title="Provider Id")
    service_id: Optional[str] = Field(title="Service Id", defalut=None)
    top_services_numb: Optional[int] = Field(title="Top Services Numb", default=5)
    top_competitors_numb: Optional[int] = Field(title="Top Competitors Numb", default=5)
