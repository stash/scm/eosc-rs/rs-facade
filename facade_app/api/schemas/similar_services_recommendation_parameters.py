# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Models for the similar services endpoint."""
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field


class SimilarServicesRecommendationParameters(BaseModel):
    """User and resource context for recommendations."""

    user_id: Optional[int] = Field(
        title="User ID",
        description="The unique marketplace identifier of the logged user.",
        default=None,
    )
    unique_id: str = Field(
        title="Not logged user ID",
        description="The unique identifier of the not logged user.",
    )
    aai_uid: Optional[str] = Field(
        title="AAI user ID",
        description="The unique AAI identifier of the logged user.",
        default=None,
    )
    timestamp: datetime = Field(
        title="Timestamp",
        description="The exact time of the recommendation request sending "
        "in iso8601 format.",
    )
    service_id: int = Field(title="Service Id")
    num: int = Field(title="Num")
