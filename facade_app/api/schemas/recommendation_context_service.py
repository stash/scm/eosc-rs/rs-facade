# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Models for the recommendations of services endpoint."""
from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, Field

from facade_app.api.schemas.recommendation_context import (
    RecommendationContext,
    SearchData,
)
from facade_app.structures.types import ClientId, PanelId


def to_int_list(elements: Optional[List[str]]) -> Optional[List[int]]:
    """Map str element in list to int

    Args:
        elements (Optional[List[str]]): Input list

    Returns:
        Optional[List[int]]: Output list
    """
    if elements:
        return [int(elem) for elem in elements if elem.isnumeric()]
    return elements


class SearchDataServices(BaseModel):
    """User search data."""

    q: Optional[str] = Field(title="Search phrase", default=None)
    categories: Optional[List[int]] = Field(title="Category", default=None)
    geographical_availabilities: Optional[List[str]] = Field(
        title="Countries", default=None
    )
    order_type: Optional[str] = Field(title="Order type", default=None)
    providers: Optional[List[int]] = Field(title="Provider", default=None)
    rating: Optional[str] = Field(title="Rating", default=None)
    related_platforms: Optional[List[int]] = Field(
        title="Related platforms", default=None
    )
    scientific_domains: Optional[List[int]] = Field(
        title="Scientific domain", default=None
    )
    sort: Optional[str] = Field(title="Sort filter", default=None)
    target_users: Optional[List[int]] = Field(title="Target users", default=None)

    @classmethod
    def from_search_data(cls, other: SearchData):
        """Create SearchDataServices object from SearchData

        Args:
            other (SearchData): Source object

        Returns:
            SearchDataServices: SearchDataServices object
        """
        return cls(
            q=other.q,
            categories=to_int_list(other.categories),
            geographical_availabilities=other.geographical_availabilities,
            order_type=other.order_type,
            providers=to_int_list(other.providers),
            rating=other.rating,
            related_platforms=to_int_list(other.related_platforms),
            scientific_domains=to_int_list(other.scientific_domains),
            sort=other.sort,
            target_users=to_int_list(other.target_users),
        )


class RecommendationContextServices(BaseModel):
    """User context for recommendations."""

    user_id: Optional[int] = Field(
        title="User ID",
        description="The unique marketplace identifier of the logged user.",
        default=None,
    )
    unique_id: str = Field(
        title="Not logged user ID",
        description="The unique identifier of the not logged user.",
    )
    aai_uid: Optional[str] = Field(
        title="AAI user ID",
        description="The unique AAI identifier of the logged user.",
        default=None,
    )
    timestamp: datetime = Field(
        title="Timestamp",
        description="The exact time of the recommendation request sending "
        "in iso8601 format.",
    )
    visit_id: str = Field(
        title="recommendation page visit ID",
        description="The unique identifier of the user presence on the "
        "recommendation page in the specific time (could be "
        "a function of above fields)",
    )
    page_id: str = Field(
        title="Page ID",
        description="The unique identifier of the page with recommendation panel.",
    )
    panel_id: PanelId = Field(
        title="Version of the panel",
        description="The unique identifiers of the set of recommender panels on the page.",
    )
    candidates: List[int] = Field(
        title="Recommendation candidates",
        description="List of candidates for recommendation",
    )
    client_id: Optional[ClientId] = Field(title="Client ID", default=None)
    engine_version: Optional[str] = Field(title="Engine version", default=None)
    search_data: SearchDataServices

    @classmethod
    def from_recommendation_context(
        cls, other: RecommendationContext, candidates: List[int]
    ):
        """Create RecommendationContextServices object from RecommendationContext

        Args:
            other (RecommendationContext): Source object
            candidates (List[str]): List of candidates

        Returns:
            RecommendationContextServices: RecommendationContextServices object
        """
        return cls(
            user_id=other.user_id,
            unique_id=other.unique_id,
            aai_uid=other.aai_uid,
            timestamp=other.timestamp,
            visit_id=other.visit_id,
            page_id=other.page_id,
            panel_id=other.panel_id,
            candidates=candidates,
            client_id=other.client_id,
            engine_version=other.engine_version,
            search_data=SearchDataServices.from_search_data(other.search_data),
        )
