# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Models for the recommendations of resources endpoint."""
from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, Field

from facade_app.api.schemas.recommendation_context import (
    RecommendationContext,
    SearchData,
)
from facade_app.structures.types import ClientId, PanelId


class RecommendationContextResources(BaseModel):
    """User context for resources recommendations."""

    user_id: Optional[int] = Field(
        title="User ID",
        description="The unique marketplace identifier of the logged user.",
        default=None,
    )
    unique_id: str = Field(
        title="Not logged user ID",
        description="The unique identifier of the not logged user.",
    )
    aai_uid: Optional[str] = Field(
        title="AAI user ID",
        description="The unique AAI identifier of the logged user.",
        default=None,
    )
    timestamp: datetime = Field(
        title="Timestamp",
        description="The exact time of the recommendation request sending "
        "in iso8601 format.",
    )
    visit_id: str = Field(
        title="recommendation page visit ID",
        description="The unique identifier of the user presence on the "
        "recommendation page in the specific time (could be "
        "a function of above fields)",
    )
    page_id: str = Field(
        title="Page ID",
        description="The unique identifier of the page with recommendation panel.",
    )
    panel_id: PanelId = Field(
        title="Version of the panel",
        description="The unique identifiers of the set of recommender panels on the page.",
    )
    candidates: List[str] = Field(
        title="Recommendation candidates",
        description="List of candidates for recommendation",
    )
    client_id: Optional[ClientId] = Field(title="Client ID", default=None)
    engine_version: Optional[str] = Field(title="Engine version", default=None)
    search_data: SearchData

    @classmethod
    def from_recommendation_context(
        cls, other: RecommendationContext, candidates: List[str]
    ):
        """Create RecommendationContextResources object from RecommendationContext

        Args:
            other (RecommendationContext): Source object
            candidates (List[str]): List of candidates

        Returns:
            RecommendationContextResources: RecommendationContextResources object
        """
        return cls(
            user_id=other.user_id,
            unique_id=other.unique_id,
            aai_uid=other.aai_uid,
            timestamp=other.timestamp,
            visit_id=other.visit_id,
            page_id=other.page_id,
            panel_id=other.panel_id,
            candidates=candidates,
            client_id=other.client_id,
            engine_version=other.engine_version,
            search_data=other.search_data,
        )
