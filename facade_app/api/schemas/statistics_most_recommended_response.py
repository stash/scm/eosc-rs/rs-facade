# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

""" Models for response for most recommended statistics."""
from pydantic import BaseModel, Field


class StatisticsMostRecommendedResponse(BaseModel):
    """Most recommended service"""

    service_id: str = Field(title="	Service Id")
    recommendations: int = Field(title="Recommendations")
