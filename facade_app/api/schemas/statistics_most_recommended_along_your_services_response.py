# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Models for response for the most recommended along your services endpoint."""
from typing import List

from pydantic import BaseModel, Field

from facade_app.api.schemas.statistics_most_recommended_response import (
    StatisticsMostRecommendedResponse,
)


class StatisticsMostRecommendedAlongYourServicesResponse(BaseModel):
    """Most recommended service along provider"""

    service_id: str = Field(title="Service Id")
    total_competitor_recommendations: int = Field(
        title="Total Competitor Recommendations"
    )
    competitors: List[StatisticsMostRecommendedResponse] = Field(title="Competitors")
