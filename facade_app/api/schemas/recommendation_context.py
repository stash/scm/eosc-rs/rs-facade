# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Models for the recommendations endpoint."""
from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel, Field

from facade_app.structures.types import ClientId, PanelId


class SearchData(BaseModel):
    """User search data."""

    q: Optional[str] = Field(title="Search phrase", default=None)
    categories: Optional[List[str]] = Field(title="Category", default=None)
    geographical_availabilities: Optional[List[str]] = Field(
        title="Countries", default=None
    )
    order_type: Optional[str] = Field(title="Order type", default=None)
    providers: Optional[List[str]] = Field(title="Provider", default=None)
    rating: Optional[str] = Field(title="Rating", default=None)
    related_platforms: Optional[List[str]] = Field(
        title="Related platforms", default=None
    )
    scientific_domains: Optional[List[str]] = Field(
        title="Scientific domain", default=None
    )
    sort: Optional[str] = Field(title="Sort filter", default=None)
    target_users: Optional[List[str]] = Field(title="Target users", default=None)


class Candidates(BaseModel):
    """Candidates to sort."""

    dataset: Optional[List[str]] = Field(
        title="Dataset candidates",
        description="List of resource ids of dataset candidates for sorting",
        default=[],
    )
    publication: Optional[List[str]] = Field(
        title="Publication candidates",
        description="List of resource ids of publication candidates for sorting",
        default=[],
    )
    software: Optional[List[str]] = Field(
        title="Software candidates",
        description="List of resource ids of software candidates for sorting",
        default=[],
    )
    other: Optional[List[str]] = Field(
        title="Other research product candidates",
        description="List of resource ids of other research product candidates for sorting",
        default=[],
    )
    training: Optional[List[str]] = Field(
        title="Training candidates",
        description="List of resource ids of training candidates for sorting",
        default=[],
    )
    service: Optional[List[int]] = Field(
        title="Service candidates",
        description="List of resource ids of service candidates for sorting",
        default=[],
    )
    datasource: Optional[List[int]] = Field(
        title="Data source candidates",
        description="List of resource ids of data source candidates for sorting",
        default=[],
    )


class RecommendationContext(BaseModel):
    """User context for recommendations."""

    user_id: Optional[int] = Field(
        title="User ID",
        description="The unique marketplace identifier of the logged user.",
        default=None,
    )
    unique_id: str = Field(
        title="Not logged user ID",
        description="The unique identifier of the not logged user.",
    )
    aai_uid: Optional[str] = Field(
        title="AAI user ID",
        description="The unique AAI identifier of the logged user.",
        default=None,
    )
    timestamp: datetime = Field(
        title="Timestamp",
        description="The exact time of the recommendation request sending "
        "in iso8601 format.",
    )
    visit_id: str = Field(
        title="recommendation page visit ID",
        description="The unique identifier of the user presence on the "
        "recommendation page in the specific time (could be "
        "a function of above fields)",
    )
    page_id: str = Field(
        title="Page ID",
        description="The unique identifier of the page with recommendation panel.",
    )
    panel_id: PanelId = Field(
        title="Version of the panel",
        description="The unique identifiers of the set of recommender panels on the page.",
    )
    candidates: Candidates = Field(
        title="Recommendation candidates",
        description="Lists of candidates for recommendation",
    )
    client_id: Optional[ClientId] = Field(title="Client ID", default=None)
    engine_version: Optional[str] = Field(title="Engine version", default=None)
    search_data: SearchData
