# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Models for the statistics for most recommended service endpoint."""
from typing import Optional

from pydantic import BaseModel, Field


class StatisticsMostRecommended(BaseModel):
    """Provider context"""

    provider_id: str = Field(title="Provider Id")
    top_n: Optional[int] = Field(title="Top N", default=5)
