# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

""" Models for response with recommendations with resources score."""
from __future__ import annotations

from typing import List, Optional

import pandas as pd

from facade_app.api.schemas.recommendation_set import RecommendationSetBase


class RecommendationSetWithScore(RecommendationSetBase):
    """Resource recommendations set with score"""

    scores: List[float] = None
    engine_version: Optional[str] = None

    def to_pandas(self) -> pd.DataFrame:
        """Convert to pandas DataFrame

        Returns:
            pd.DataFrame: pandas DataFrame
        """
        return pd.DataFrame(
            [
                self.recommendations,
                self.explanations,
                self.explanations_short,
                self.scores,
            ],
            index=["recommendations", "explanations", "explanations_short", "score"],
        ).T

    def from_pandas(self, other_df: pd.DataFrame):
        """Import data from pandas DataFrame

        Args:
            other_df (pd.DataFrame): source DataFrame
        """
        self.recommendations = other_df.recommendations.to_list()
        self.explanations = other_df.explanations.to_list()
        self.explanations_short = other_df.explanations_short.to_list()
        self.scores = other_df.score.to_list()

    def sort(self, take: int):
        """Sort resources and take top with best score

        Args:
            take (int): Number of resources to take
        """
        dataframe = self.to_pandas()
        dataframe.sort_values("score", inplace=True, ascending=False)
        if take > 0:
            dataframe = dataframe.head(take)
        self.from_pandas(dataframe)

    def add_other(self, other: RecommendationSetWithScore):
        """Add to recommendation other RecommendationSetWithScore object

        Args:
            other (RecommendationSetWithScore): Object to add
        """
        self.recommendations += other.recommendations
        self.explanations += other.explanations
        self.explanations_short += other.explanations_short
        self.scores += other.scores
