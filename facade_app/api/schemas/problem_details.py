# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Definition of error structure returned by endpoints."""
from typing import Optional, Union

from pydantic import BaseModel


class ProblemDetails(BaseModel):
    """API error or problem details."""

    type: Optional[str] = None
    title: Optional[str] = None
    status: int
    detail: Union[str, dict, None]
    instance: Optional[str] = None
    component: Optional[str] = None
