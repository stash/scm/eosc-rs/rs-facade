# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""Models for the daily statistics endpoint."""
from pydantic import BaseModel, Field


class StatisticsDaily(BaseModel):
    """Provider and service context"""

    provider_id: str = Field(title="Provider Id")
    service_id: str = Field(title="Service Id")
