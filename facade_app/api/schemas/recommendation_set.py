# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

""" Models for response with recommendations."""
from __future__ import annotations

from typing import List, Union

from pydantic import BaseModel

from facade_app.structures.types import PanelId


class RecommendationSetBase(BaseModel):
    """Resource recommendations set base"""

    panel_id: PanelId
    recommendations: List[Union[int, str]]
    explanations: List[str]
    explanations_short: List[str]


class RecommendationSet(RecommendationSetBase):
    """Resource recommendations set"""

    engine_versions: List[str]

    @classmethod
    def from_other(cls, other: RecommendationSetBase):
        """Create RecommendationSet object from RecommendationSetBase

        Args:
            other (RecommendationSetBase):  Source object

        Returns:
            RecommendationSet: RecommendationSet object
        """
        return cls(
            panel_id=other.panel_id,
            recommendations=other.recommendations,
            explanations=other.explanations,
            explanations_short=other.explanations_short,
            engine_versions=[],
        )
