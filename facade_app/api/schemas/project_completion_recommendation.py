# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

""" Models for response for project completion."""
from pydantic import BaseModel, Field


class ProjectCompletionRecommendation(BaseModel):
    """Service with score"""

    service_id: int = Field(title="Service Id")
    score: float = Field(title="Score")
