"""Endpoint definition for diagnostic"""
import logging
import asyncio
from typing import Dict

import httpx
from fastapi import APIRouter, status
from facade_app.jms_connector import JmsConnector


from facade_app.settings import settings_default, API_VERSION

api_router = APIRouter()
logger = logging.getLogger(__name__)

jms_connector = JmsConnector()


def filter_json(input_json: Dict) -> Dict:
    """Filter nested field in dict

    Args:
        input_json (Dict): Dict to filter

    Returns:
        Dict: Filtered dict
    """
    return {
        key: value
        for key, value in input_json.items()
        if type(value) not in [dict, list]
    }


async def health_check(url: str, service_name: str, data: dict, details: bool):
    """Function that check status of depended module.

    Args:
        url (str): Url of depended module
        service_name (str): Name of the service
        data (dict): Object to store responses from depended modules
        details (bool): Parameter to request a detailed response
    """
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                url, timeout=settings_default.EXTERNAL_API_TIMEOUT_DIAG
            )
            if response.status_code == 200:
                if details:
                    data[service_name] = response.json()
                else:
                    data[service_name] = filter_json(response.json())
            else:
                message = f"Error while connecting to {service_name}: {str(response.status_code)}"
                data[service_name] = {
                    "status": "DOWN",
                    "error": message,
                }
    except httpx.ReadTimeout as exc:
        data[service_name] = {
            "status": "DOWN",
            "error": f"Error while connecting to {service_name}: Timeout {str(exc)}",
        }
    except httpx.RequestError as exc:
        data[service_name] = {
            "status": "DOWN",
            "error": f"Error while connecting to {service_name}: {str(exc)}",
        }


async def health_check_jms(data: dict):
    """Function that check status of JMS.

    Args:
        data (dict): Object to store responses from depended modules
    """
    data["JMS"] = await jms_connector.health_check()


@api_router.get(
    "/diag",
    status_code=status.HTTP_200_OK,
    tags=["diagnose"],
    summary="Diagnostic endpoint",
    description="A function to check the status of dependent services.",
)
async def diag(details: bool = False) -> Dict:
    """Function to check the diagnostics of the module and dependent modules

    Args:
        details (bool, optional): Parameter to request a detailed response. Defaults to False.

    Returns:
        Dict: Status of the module
    """
    data = {"status": "UP", "version": API_VERSION}
    await asyncio.gather(
        health_check(
            settings_default.ONLINE_ENGINE_URL + "/diag", "Online engine", data, details
        ),
        health_check(
            settings_default.CYFRONET_RS_URL + "/health",
            "Marketplace RS",
            data,
            details,
        ),
        health_check(
            settings_default.CONTENT_BASED_ENGINE_URL + "/v1/health",
            "Content-based engine",
            data,
            details,
        ),
        health_check(
            settings_default.AUTO_COMPLETION_URL + "/v1/health",
            "Auto-completion",
            data,
            details,
        ),
        health_check_jms(data),
        health_check(
            settings_default.PREPROCESSOR_URL + "/actuator/health",
            "Preprocessor",
            data,
            details,
        ),
        health_check(
            settings_default.NEARLINE_ENGINE_URL + "/diag",
            "Nearline engine",
            data,
            details,
        ),
        health_check(
            settings_default.NEAREST_NEIGHBOR_FINDER_URL + "/diag",
            "Nearest neighbor finder",
            data,
            details,
        ),
        health_check(
            settings_default.NEAREST_NEIGHBOR_FINDER_TRAINING_MODULE_URL + "/diag",
            "Nearest neighbor finder training module",
            data,
            details,
        ),
        health_check(
            settings_default.PROVIDER_INSIGHTS_URL + "/v1/health",
            "Provider insights",
            data,
            details,
        ),
    )
    return data
