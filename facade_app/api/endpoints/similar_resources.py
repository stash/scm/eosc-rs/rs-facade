"""Endpoint definition for similar resources"""

import json
import logging
from typing import Dict

from fastapi import APIRouter, BackgroundTasks, status
from fastapi.encoders import jsonable_encoder
from pydantic import parse_obj_as
import requests

from facade_app.api.schemas.similar_resources_context import SimilarResourcesContext
from facade_app.api.schemas.recommendation_set_with_score import (
    RecommendationSetWithScore,
)
from facade_app.api.schemas.recommendation_set import RecommendationSet
from facade_app.exceptions import RemoteApiUnavailableError
from facade_app.jms_connector import JmsConnector

from facade_app.settings import settings_default
from facade_app.utils import parse_error_response_details

api_router = APIRouter()
logger = logging.getLogger(__name__)
jms_connector = JmsConnector()


def write_notification(jms_message: Dict):
    """Write notification to JMS

    Args:
        jms_message (Dict): Message to send
    """
    logger.info(f"jms_message: {json.dumps(jms_message, sort_keys=True, default=str)}")

    jms_connector.send(jms_message)


@api_router.post(
    "/similar_resources",
    response_model=RecommendationSet,
    status_code=status.HTTP_200_OK,
    tags=["similar resources"],
    summary=f"""Returns a set of recommendations that contains
        {settings_default.NUMBER_OF_RECOMMENDATION} ids of recommended resources.""",
    description="""A function that generates recommendations of similar resources
    for a user based on the user's context.""",
)
async def recommendations(
    request: SimilarResourcesContext, background_tasks: BackgroundTasks
) -> RecommendationSet:
    """A function that generates recommendations of similar resources for a user
        based on the user's context.

    Args:
        request (SimilarResourcesContext): the context of the user and the resource for which
        the recommendation is created.

    Raises:
        RemoteApiUnavailableError: Remote API unavailable error


    Returns:
        RecommendationSet: A collection of recommended resources.
    """
    logging.info(json.dumps(request.__dict__, sort_keys=True, default=str))
    request_data = jsonable_encoder(request, exclude_none=True)
    url = settings_default.ONLINE_ENGINE_URL + "/similar_resources"

    try:
        response = requests.post(
            url, json=request_data, timeout=settings_default.EXTERNAL_API_TIMEOUT
        )
        if response.status_code != 200:
            details = parse_error_response_details(response)
            logger.error(f"Error connecting to Online_engine: {details}")
            raise RemoteApiUnavailableError(
                "Online_engine",
                str(response.url),
                response.status_code,
                details,
            )
    except requests.exceptions.ConnectionError as exc:
        logger.error(f"Error connecting to Online_engine: {exc}")
        raise RemoteApiUnavailableError(
            "Online_engine",
            url,
            408,
            str(exc),
        ) from exc

    recommendation_set_with_score = parse_obj_as(
        RecommendationSetWithScore, json.loads(response.text)
    )
    recommendation_set = RecommendationSet.from_other(recommendation_set_with_score)
    recommendation_set.engine_versions = [recommendation_set_with_score.engine_version]
    jms_message = {"context": jsonable_encoder(request), "panel_id": request.panel_id}
    jms_message["recommendations"] = json.loads(recommendation_set.json())[
        "recommendations"
    ]
    jms_message["recommender_systems"] = ["Online_engine"]
    background_tasks.add_task(write_notification, json.dumps(jms_message))

    return recommendation_set
