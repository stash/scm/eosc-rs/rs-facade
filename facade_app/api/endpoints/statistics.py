"""Endpoint definition for service statistics"""
import json
import logging
from typing import List

import requests
from fastapi import APIRouter, status
from fastapi.encoders import jsonable_encoder
from pydantic import parse_obj_as

from facade_app.api.schemas.statistics_daily import StatisticsDaily
from facade_app.api.schemas.statistics_daily_response import StatisticsDailyResponse
from facade_app.api.schemas.statistics_most_recommended import StatisticsMostRecommended
from facade_app.api.schemas.statistics_most_recommended_response import (
    StatisticsMostRecommendedResponse,
)
from facade_app.api.schemas.statistics_most_recommended_along_your_services import (
    StatisticsMostRecommendedAlongYourServices,
)
from facade_app.api.schemas.statistics_most_recommended_along_your_services_response import (
    StatisticsMostRecommendedAlongYourServicesResponse,
)
from facade_app.exceptions import RemoteApiUnavailableError
from facade_app.settings import settings_default
from facade_app.utils import parse_error_response_details

api_router = APIRouter()
logger = logging.getLogger(__name__)


@api_router.post(
    "/v1/statistics/rs/daily",
    response_model=List[StatisticsDailyResponse],
    status_code=status.HTTP_200_OK,
    tags=["statistics"],
    summary="Get Daily Recommendations",
    description="A function that get daily recommendations statistics.",
)
async def statistics_rs_daily(
    request: StatisticsDaily,
) -> List[StatisticsDailyResponse]:
    """Get Daily Recommendations

    Args:
        request (StatisticsDaily): Context of the provider and service

    Raises:
        RemoteApiUnavailableError: Remote API unavailable error

    Returns:
        List[StatisticsDailyResponse]: List of daily statistics
    """
    logger.info(json.dumps(request.__dict__, sort_keys=True, default=str))
    json_compatible_item_data = jsonable_encoder(request)
    try:
        response = requests.post(
            settings_default.PROVIDER_INSIGHTS_URL + "/v1/statistics/rs/daily",
            json=json_compatible_item_data,
            timeout=settings_default.EXTERNAL_API_TIMEOUT,
        )
        if response.status_code != 200:
            details = parse_error_response_details(response)
            logger.error(f"Error connecting to Provider Insights: {details}")
            raise RemoteApiUnavailableError(
                "Provider Insights",
                str(response.url),
                response.status_code,
                details,
            )
    except requests.exceptions.ConnectionError as exc:
        logger.error(f"Error connecting to Provider Insights: {exc}")
        raise RemoteApiUnavailableError(
            "Provider Insights",
            settings_default.PROVIDER_INSIGHTS_URL + "/v1/statistics/rs/daily",
            408,
            str(exc),
        ) from exc
    response_json = json.loads(response.text)
    recommendations = parse_obj_as(List[StatisticsDailyResponse], response_json)
    return recommendations


@api_router.post(
    "/v1/statistics/rs/most_recommended/",
    response_model=List[StatisticsMostRecommendedResponse],
    status_code=status.HTTP_200_OK,
    tags=["statistics"],
    summary="Get Total Recommendations",
    description="A function that get most recommended services.",
)
async def statistics_rs_most_recommended(
    request: StatisticsMostRecommended,
) -> List[StatisticsMostRecommendedResponse]:
    """Get most recommended services

    Args:
        request (StatisticsMostRecommended): Context of the provider

    Raises:
        RemoteApiUnavailableError: Remote API unavailable error

    Returns:
        List[StatisticsMostRecommendedResponse]: List of most recommended services
    """
    logger.info(json.dumps(request.__dict__, sort_keys=True, default=str))
    json_compatible_item_data = jsonable_encoder(request)
    try:
        response = requests.post(
            settings_default.PROVIDER_INSIGHTS_URL
            + "/v1/statistics/rs/most_recommended/",
            json=json_compatible_item_data,
            timeout=settings_default.EXTERNAL_API_TIMEOUT,
        )
        if response.status_code != 200:
            details = parse_error_response_details(response)
            logger.error(f"Error connecting to Provider Insights: {details}")
            raise RemoteApiUnavailableError(
                "Provider Insights",
                str(response.url),
                response.status_code,
                details,
            )
    except requests.exceptions.ConnectionError as exc:
        logger.error(f"Error connecting to Provider Insights: {exc}")
        raise RemoteApiUnavailableError(
            "Provider Insights",
            settings_default.PROVIDER_INSIGHTS_URL
            + "/v1/statistics/rs/most_recommended/",
            408,
            str(exc),
        ) from exc
    response_json = json.loads(response.text)
    recommendations = parse_obj_as(
        List[StatisticsMostRecommendedResponse], response_json
    )
    return recommendations


@api_router.post(
    "/v1/statistics/rs/most_recommended_along_your_services/",
    response_model=List[StatisticsMostRecommendedAlongYourServicesResponse],
    status_code=status.HTTP_200_OK,
    tags=["statistics"],
    summary="Get Most Recommended Along Your Services",
    description="A function that get most recommended services along provider.",
)
async def statistics_rs_most_recommended_along_your_services(
    request: StatisticsMostRecommendedAlongYourServices,
) -> List[StatisticsMostRecommendedAlongYourServicesResponse]:
    """Get most recommended services along provider

    Args:
        request (StatisticsMostRecommendedAlongYourServices):
            Context of the provider and service

    Raises:
        RemoteApiUnavailableError: Remote API unavailable error

    Returns:
        List[StatisticsMostRecommendedAlongYourServicesResponse]:
            Statistics for provider
    """
    logger.info(json.dumps(request.__dict__, sort_keys=True, default=str))
    json_compatible_item_data = jsonable_encoder(request)
    try:
        response = requests.post(
            settings_default.PROVIDER_INSIGHTS_URL
            + "/v1/statistics/rs/most_recommended_along_your_services/",
            json=json_compatible_item_data,
            timeout=settings_default.EXTERNAL_API_TIMEOUT,
        )
        if response.status_code != 200:
            details = parse_error_response_details(response)
            logger.error(f"Error connecting to Provider Insights: {details}")
            raise RemoteApiUnavailableError(
                "Provider Insights",
                str(response.url),
                response.status_code,
                details,
            )
    except requests.exceptions.ConnectionError as exc:
        logger.error(f"Error connecting to Provider Insights: {exc}")
        raise RemoteApiUnavailableError(
            "Provider Insights",
            settings_default.PROVIDER_INSIGHTS_URL
            + "/v1/statistics/rs/most_recommended_along_your_services/",
            408,
            str(exc),
        ) from exc
    response_json = json.loads(response.text)
    recommendations = parse_obj_as(
        List[StatisticsMostRecommendedAlongYourServicesResponse], response_json
    )
    return recommendations
