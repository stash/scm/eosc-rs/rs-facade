"""Endpoint definition for project completion"""

import json
import logging
from typing import Dict, List

import requests
from fastapi import APIRouter, BackgroundTasks, status
from fastapi.encoders import jsonable_encoder
from pydantic import parse_obj_as

from facade_app.api.schemas.project_completion_recommendation import (
    ProjectCompletionRecommendation,
)
from facade_app.api.schemas.project_completion_recommendation_parameters import (
    ProjectCompletionRecommendationParameters,
)
from facade_app.exceptions import RemoteApiUnavailableError
from facade_app.jms_connector import JmsConnector
from facade_app.settings import settings_default
from facade_app.utils import parse_error_response_details

api_router = APIRouter()
logger = logging.getLogger(__name__)
jms_connector = JmsConnector()


def write_notification(jms_message: Dict):
    """Write notification to JMS

    Args:
        jms_message (Dict): Message to send
    """
    logger.info(f"jms_message: {json.dumps(jms_message, sort_keys=True, default=str)}")

    jms_connector.send(jms_message)


@api_router.post(
    "/project_completion/recommendation",
    response_model=List[ProjectCompletionRecommendation],
    status_code=status.HTTP_200_OK,
    tags=["recommendation"],
    summary="Get Project Completion Recommendation",
    description="""**Suggest a completion for the project**

        Based on the project given as input, we recommend services that are frequently combined
        with the ones existing in the project.

        - **project_id**: the id of the project currently viewed by the user
        - **num**: number of recommendations we want returned

        **Returns** a list of dicts where service_id is the id of the recommended service
        and score is the support from the frequent item sets.""",
)
async def project_completion_recommendation(
    request: ProjectCompletionRecommendationParameters,
    background_tasks: BackgroundTasks,
) -> List[ProjectCompletionRecommendation]:
    """Suggest a completion for the project

    Args:
        request (ProjectCompletionRecommendationParameters): The context of the project
        for which the recommendation is created.

    Raises:
        RemoteApiUnavailableError: Remote API unavailable error

    Returns:
        List[ProjectCompletionRecommendation]: A collection of recommended resources.
    """
    logger.info(json.dumps(request.__dict__, sort_keys=True, default=str))
    json_compatible_item_data = jsonable_encoder(request)
    try:
        response = requests.post(
            settings_default.AUTO_COMPLETION_URL
            + "/v1/project_completion/recommendation",
            json=json_compatible_item_data,
            timeout=settings_default.EXTERNAL_API_TIMEOUT,
        )
        if response.status_code != 200:
            details = parse_error_response_details(response)
            logger.error(f"Error connecting to Auto-completion: {details}")
            raise RemoteApiUnavailableError(
                "Auto-completion",
                str(response.url),
                response.status_code,
                details,
            )
    except requests.exceptions.ConnectionError as exc:
        logger.error(f"Error connecting to Auto-completion: {exc}")
        raise RemoteApiUnavailableError(
            "Auto-completion",
            settings_default.AUTO_COMPLETION_URL
            + "/v1/project_completion/recommendation",
            408,
            str(exc),
        ) from exc
    response_json = json.loads(response.text)
    recommendations = parse_obj_as(List[ProjectCompletionRecommendation], response_json)
    jms_message = {"context": jsonable_encoder(request), "panel_id": "Auto-completion"}
    jms_message["recommendations"] = response_json
    jms_message["recommender_systems"] = ["Auto-completion"]
    background_tasks.add_task(write_notification, json.dumps(jms_message))
    return recommendations
