"""Endpoint definition for similar services"""

import json
import logging
from typing import Dict

import requests
from fastapi import APIRouter, BackgroundTasks, status
from fastapi.encoders import jsonable_encoder
from pydantic import parse_obj_as

from facade_app.api.schemas.recommendation_set import RecommendationSet
from facade_app.api.schemas.recommendation_set_with_score import (
    RecommendationSetWithScore,
)
from facade_app.api.schemas.similar_services_recommendation_parameters import (
    SimilarServicesRecommendationParameters,
)
from facade_app.exceptions import RemoteApiUnavailableError
from facade_app.jms_connector import JmsConnector
from facade_app.settings import settings_default
from facade_app.utils import parse_error_response_details

api_router = APIRouter()
logger = logging.getLogger(__name__)
jms_connector = JmsConnector()


def write_notification(jms_message: Dict):
    """Write notification to JMS

    Args:
        jms_message (Dict): Message to send
    """
    logger.info(f"jms_message: {json.dumps(jms_message, sort_keys=True, default=str)}")

    jms_connector.send(jms_message)


@api_router.post(
    "/similar_services/recommendation",
    response_model=RecommendationSet,
    status_code=status.HTTP_200_OK,
    tags=["recommendation"],
    summary="Get Similar Services Recommendation",
    description="""**Suggest a similar service**

        Based on the service given as input, we recommend similar services utilizing both textual
        and metadata attributes.

        - **user_id**: the id of the user (as it was given in the marketplace)
        - **unique_id**: the unique identifier of the not logged in user
        - **aai_uid**: the unique AAI identifier of the logged user
        - **timestamp** the exact time of the recommendation request sending
        - **service_id**: the id of the service currently viewed by the user
        - **num**: number of recommendations we want returned

        **Returns** a collection of recommended services.""",
)
async def similar_services_recommendation(
    request: SimilarServicesRecommendationParameters,
    background_tasks: BackgroundTasks,
) -> RecommendationSet:
    """Suggest a similar service

    Args:
        request (SimilarServicesRecommendationParameters): The context of the user and service
        for which the recommendation is created.

    Raises:
        RemoteApiUnavailableError: Remote API unavailable error

    Returns:
        RecommendationSet:  A collection of recommended services.
    """
    logger.info(json.dumps(request.__dict__, sort_keys=True, default=str))
    json_compatible_item_data = jsonable_encoder(request)
    try:
        response = requests.post(
            settings_default.CONTENT_BASED_ENGINE_URL
            + "/v1/similar_services/recommendation",
            json=json_compatible_item_data,
            timeout=settings_default.EXTERNAL_API_TIMEOUT,
        )
        if response.status_code != 200:
            details = parse_error_response_details(response)
            logger.error(f"Error connecting to Content-based engine: {details}")

            raise RemoteApiUnavailableError(
                "Content-based engine",
                str(response.url),
                response.status_code,
                details,
            )
    except requests.exceptions.ConnectionError as exc:
        logger.error(f"Error connecting to Content-based engine: {exc}")
        raise RemoteApiUnavailableError(
            "Content-based engine",
            settings_default.CONTENT_BASED_ENGINE_URL
            + "/v1/similar_services/recommendation",
            408,
            str(exc),
        ) from exc
    response_json = response.json()
    recommendations = parse_obj_as(RecommendationSetWithScore, response_json)
    engine_version = recommendations.engine_version
    recommendations = RecommendationSet.from_other(recommendations)
    recommendations.engine_versions.append(engine_version)
    jms_message = {
        "context": jsonable_encoder(request),
        "panel_id": recommendations.panel_id,
    }
    jms_message["recommendations"] = recommendations.recommendations
    jms_message["recommender_systems"] = ["Content-based engine"]

    background_tasks.add_task(write_notification, json.dumps(jms_message))
    return recommendations
