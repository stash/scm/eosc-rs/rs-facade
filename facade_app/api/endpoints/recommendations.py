""" Endpoint definition for personalized and anonymous recommendations.
"""

import json
import logging
from typing import Dict

from fastapi import APIRouter, BackgroundTasks, status
from fastapi.encoders import jsonable_encoder

from facade_app.api.schemas.recommendation_context import RecommendationContext
from facade_app.api.schemas.recommendation_set import RecommendationSet
from facade_app.engines.engine_recommendation import engine_recommendation
from facade_app.jms_connector import JmsConnector

api_router = APIRouter()
logger = logging.getLogger(__name__)

jms_connector = JmsConnector()


def write_notification(jms_message: Dict):
    """Write notification to JMS

    Args:
        jms_message (Dict): Message to send
    """
    logger.info(f"jms_message: {json.dumps(jms_message, sort_keys=True, default=str)}")

    jms_connector.send(jms_message)


@api_router.post(
    "/recommendations",
    response_model=RecommendationSet,
    status_code=status.HTTP_200_OK,
    tags=["recommendation"],
    summary="""Returns a list of sets of recommendations for each of num_lists list.
        Each of the set of recommendations contains 10 ids of recommended scientific services or resources,
        explanations, and short explanations.""",
    description="""A function that generates personalized and anonymous recommendations
    for the user based on the user context.""",
)
async def recommendations(
    request: RecommendationContext,
    background_tasks: BackgroundTasks,
) -> RecommendationSet:
    """A function that generates personalized and anonymous recommendations for the user
        based on the user context.

    Args:
        request (RecommendationContext): The context of the user for whom the recommendation
        is created.
        background_tasks (BackgroundTasks): Background tasks used to send notification to JMS

    Returns:
        RecommendationSet: A collection of recommended resources.
    """
    logger.info(json.dumps(request.__dict__, sort_keys=True, default=str))

    context_json = jsonable_encoder(request)

    jms_message = {"context": context_json, "panel_id": request.panel_id}

    (
        recommendation_set,
        sources,
        recommender_systems,
    ) = await engine_recommendation.recommendations(request)

    jms_message["recommendations"] = json.loads(recommendation_set.json())[
        "recommendations"
    ]
    jms_message["recommender_systems"] = recommender_systems

    if len(sources) > 1:
        jms_message["sources"] = sources

    background_tasks.add_task(write_notification, json.dumps(jms_message))

    return recommendation_set
