# pylint: disable=no-name-in-module, no-self-argument, too-few-public-methods

"""A module with configuration of application."""
from pydantic_settings import BaseSettings, SettingsConfigDict

from facade_app.structures.types import AggregationType
from facade_app.utils import get_project_root

API_VERSION = "1.5.4"


class Settings(BaseSettings):
    """Class to store all application settings."""

    model_config = SettingsConfigDict(
        env_file=str(get_project_root() / ".env"), env_file_encoding="utf-8"
    )
    PROJECT_NAME: str = "RS Facade"
    API_STR: str = "/api"
    SAVE_SWAGGER: str = "no"
    LOG_LEVEL: str = "DEBUG"
    ONLINE_ENGINE_URL: str = "http://127.0.0.1:8001"
    CYFRONET_RS_URL: str = "http://127.0.0.1:8001"
    CONTENT_BASED_ENGINE_URL: str = "http://127.0.0.1:8002"
    PREPROCESSOR_URL: str = "http://127.0.0.1:8002"
    NEARLINE_ENGINE_URL: str = "http://127.0.0.1:8002"
    NEAREST_NEIGHBOR_FINDER_URL: str = "http://127.0.0.1:8002"
    NEAREST_NEIGHBOR_FINDER_TRAINING_MODULE_URL: str = "http://127.0.0.1:8002"
    PROVIDER_INSIGHTS_URL: str = "http://127.0.0.1:8002"
    AUTO_COMPLETION_URL: str = "http://127.0.0.1:8002"
    JMS_URL: str = "127.0.0.1"
    JMS_PORT: str = 8001
    JMS_USER: str = "admin"
    JMS_PASSWORD: str = "admin"
    JMS_RECOMMENDATION_TOPIC: str = "/topic/recommendations"
    SSL: bool = False

    EXTERNAL_API_TIMEOUT: float = 2
    EXTERNAL_API_TIMEOUT_DIAG: float = 10

    ENABLE_AIOHTTP_TRACING: str = "no"
    NUMBER_OF_RECOMMENDATION: int = 10
    AGGREGATION_TYPE: AggregationType = "top"


settings_default = Settings()

tags_metadata = [
    {
        "name": "recommendation",
        "description": "Endpoint used for getting recommendations",
    }
]
