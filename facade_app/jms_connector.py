"""Module responsible for communication with JMS"""
import logging
import time
from ssl import SSLError
from typing import Dict

import stomp
from stomp.exception import ConnectFailedException, NotConnectedException

from facade_app.settings import settings_default

logger = logging.getLogger(__name__)


class JmsConnector:
    """Class responsible for communication with JMS"""

    def __init__(
        self,
        jms_url: str = settings_default.JMS_URL,
        jms_port: str = settings_default.JMS_PORT,
        jms_password: str = settings_default.JMS_PASSWORD,
        jms_user: str = settings_default.JMS_USER,
        jms_recommendation_topic: str = settings_default.JMS_RECOMMENDATION_TOPIC,
        ssl: bool = settings_default.SSL,
    ):
        self.jms_connector = None
        self.jms_url = jms_url
        self.jms_port = jms_port
        self.jms_password = jms_password
        self.jms_user = jms_user
        self.jms_recommendation_topic = jms_recommendation_topic
        self.ssl = ssl

    def send(self, jms_message: Dict) -> bool:
        """Send message to JMS

        Args:
            jms_message (Dict): Message to send

        Returns:
            bool: True if success else False
        """
        success = False
        try:
            self.jms_connector = stomp.Connection([(self.jms_url, self.jms_port)])
            if self.ssl:
                self.jms_connector.set_ssl([(self.jms_url, self.jms_port)])
            self.jms_connector.connect(self.jms_user, self.jms_password, wait=True)
            if not self.jms_connector.is_connected():
                logger.warning("Exception connecting to JMS")
                return False
            self.jms_connector.send(
                self.jms_recommendation_topic, str(jms_message).encode("utf-8")
            )
            success = True
        except (
            ConnectFailedException,
            NotConnectedException,
            BrokenPipeError,
            AttributeError,
            ConnectionResetError,
            SSLError,
        ) as exc:
            logger.warning(
                f"Exception connecting to JMS {exc.__class__.__name__}: {exc}"
            )

        except Exception as exc:
            logger.warning(
                f"Exception connecting to JMS {exc.__class__.__name__}: {exc}"
            )
        finally:
            time.sleep(1)
            self.jms_connector.disconnect()
            logger.info("Connection to JMS closed.")
        return success

    async def health_check(self) -> Dict:
        """Function that check status of JMS.

        Returns:
            dict: set of information on whether it was possible to connect to JMS.
        """

        response = {"status": "DOWN"}
        try:
            jms_connector = stomp.Connection(
                [(settings_default.JMS_URL, settings_default.JMS_PORT)]
            )
            if settings_default.SSL:
                jms_connector.set_ssl(
                    [(settings_default.JMS_URL, settings_default.JMS_PORT)]
                )
            jms_connector.connect(
                settings_default.JMS_USER, settings_default.JMS_PASSWORD
            )
            response = {
                "status": "UP",
            }
        except (
            ConnectFailedException,
            NotConnectedException,
            BrokenPipeError,
            AttributeError,
            ConnectionResetError,
            SSLError,
        ) as exc:
            response = {
                "status": "DOWN",
                "error": f"Error while connecting to JMS {exc.__class__.__name__}: {str(exc)}",
            }
        except Exception as exc:
            response = {
                "status": "DOWN",
                "error": f"Error while connecting to JMS {exc.__class__.__name__}: {str(exc)}",
            }
        finally:
            time.sleep(1)
            jms_connector.disconnect()
            logger.info("Connection to JMS closed.")
        return response
