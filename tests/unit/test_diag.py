from unittest.mock import MagicMock

from asynctest import patch

from facade_app.api.endpoints.diagnostic import diag, health_check
from tests.base_test_api_helper import BaseTestApi


class TestDiag(BaseTestApi):
    @patch("httpx.AsyncClient.get")
    async def test_diag(self, mock_get):
        mock_get.return_value.status_code = 200
        mock_get.return_value.json = MagicMock()
        mock_get.return_value.json.return_value = {"status": "DOWN"}

        response = await diag()
        self.assertEqual(response["status"], "UP")

    @patch("httpx.AsyncClient.get")
    async def test_health_check(self, mock_get):
        mock_get.return_value.status_code = 200
        mock_get.return_value.json = MagicMock()
        mock_get.return_value.json.return_value = {"status": "UP", "data": {}}
        data = {}
        service_name = "test_service"
        await health_check("", service_name, data, False)
        self.assertEqual(data[service_name]["status"], "UP")
        self.assertNotIn("data", data[service_name])

        await health_check("", service_name, data, True)
        self.assertEqual(data[service_name]["status"], "UP")
        self.assertIn("data", data[service_name])

    @patch("httpx.AsyncClient.get")
    async def test_health_check_error(self, mock_get):
        mock_get.return_value.status_code = 400
        mock_get.return_value.json = MagicMock()
        mock_get.return_value.json.return_value = {"message": "error"}
        data = {}
        service_name = "test_service"
        await health_check("", service_name, data, False)
        self.assertEqual(data[service_name]["status"], "DOWN")
