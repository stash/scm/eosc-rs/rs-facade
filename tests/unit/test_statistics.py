import unittest
from unittest import mock

from fastapi.encoders import jsonable_encoder

from facade_app.api.endpoints.statistics import (
    statistics_rs_daily,
    statistics_rs_most_recommended,
    statistics_rs_most_recommended_along_your_services,
)
from facade_app.api.schemas.statistics_daily import StatisticsDaily
from facade_app.api.schemas.statistics_daily_response import StatisticsDailyResponse
from facade_app.api.schemas.statistics_most_recommended import StatisticsMostRecommended
from facade_app.api.schemas.statistics_most_recommended_along_your_services import (
    StatisticsMostRecommendedAlongYourServices,
)
from facade_app.api.schemas.statistics_most_recommended_response import (
    StatisticsMostRecommendedResponse,
)
from facade_app.exceptions import RemoteApiUnavailableError
from tests.unit.mock_response import MockResponse, mocked_requests_post_error


def mocked_daily_post(*args, **kwargs):
    return MockResponse(
        [{"date": "2013-02-24T12:28:45.610Z", "recommendations": 2}], 200
    )


def mocked_most_recommended_post(*args, **kwargs):
    return MockResponse([{"service_id": "1", "recommendations": 2}], 200)


def mocked_most_recommended_along_your_services_post(*args, **kwargs):
    return MockResponse(
        [
            {
                "service_id": "1",
                "total_competitor_recommendations": 12,
                "competitors": [{"service_id": "1", "recommendations": 1}],
            }
        ],
        200,
    )


class TestStatistics(unittest.IsolatedAsyncioTestCase):
    @mock.patch(
        "requests.post",
        side_effect=mocked_daily_post,
    )
    async def test_daily(self, mock_post):
        params = StatisticsDaily(provider_id="1", service_id="1")

        response = await statistics_rs_daily(params)
        self.assertEqual(
            response,
            [
                StatisticsDailyResponse(
                    date="2013-02-24T12:28:45.610Z", recommendations=2
                )
            ],
        )

    @mock.patch(
        "requests.post",
        side_effect=mocked_requests_post_error,
    )
    async def test_daily_error(self, mock_post):
        params = StatisticsDaily(provider_id="1", service_id="1")
        with self.assertRaises(RemoteApiUnavailableError):
            await statistics_rs_daily(params)

    @mock.patch(
        "requests.post",
        side_effect=mocked_most_recommended_post,
    )
    async def test_most_recommended(self, mock_post):
        params = StatisticsMostRecommended(provider_id="1", top_n=1)

        response = await statistics_rs_most_recommended(params)
        self.assertEqual(
            response,
            [StatisticsMostRecommendedResponse(service_id="1", recommendations=2)],
        )

    @mock.patch(
        "requests.post",
        side_effect=mocked_requests_post_error,
    )
    async def test_most_recommended_error(self, mock_post):
        params = StatisticsMostRecommended(provider_id="1", top_n=1)
        with self.assertRaises(RemoteApiUnavailableError):
            await statistics_rs_most_recommended(params)

    @mock.patch(
        "requests.post",
        side_effect=mocked_most_recommended_along_your_services_post,
    )
    async def test_most_recommended_along_your_services(self, mock_post):
        params = StatisticsMostRecommendedAlongYourServices(
            provider_id="1", service_id="1", top_competitors_numb=1, top_services_numb=1
        )

        response = await statistics_rs_most_recommended_along_your_services(params)
        self.assertEqual(
            jsonable_encoder(response),
            [
                {
                    "service_id": "1",
                    "total_competitor_recommendations": 12,
                    "competitors": [{"service_id": "1", "recommendations": 1}],
                }
            ],
        )

    @mock.patch(
        "requests.post",
        side_effect=mocked_requests_post_error,
    )
    async def test_most_recommended_along_your_services_error(self, mock_post):
        params = StatisticsMostRecommendedAlongYourServices(
            provider_id="1", service_id="1", top_competitors_numb=1, top_services_numb=1
        )
        with self.assertRaises(RemoteApiUnavailableError):
            await statistics_rs_most_recommended_along_your_services(params)
