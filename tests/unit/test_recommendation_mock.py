import json
from typing import Tuple
from unittest.mock import MagicMock

from asynctest import patch

from facade_app.api.endpoints.recommendations import recommendations
from facade_app.api.schemas.recommendation_context import (
    Candidates,
    RecommendationContext,
    SearchData,
)
from facade_app.api.schemas.recommendation_set import RecommendationSet
from facade_app.settings import settings_default
from tests.base_test_api_helper import BaseTestApi


class TestRecomendationMock(BaseTestApi):
    async def get_resource(
        self, mock_post, panel_id: str, resource_type: str, engine_version: str
    ) -> Tuple[RecommendationSet, int]:
        return_json, number_of_resources, _ = self.prepare_response(
            panel_id, resource_type, engine_version
        )

        mock_post.return_value.json = MagicMock()
        mock_post.return_value.status_code = 200
        mock_post.return_value.json.return_value = return_json
        background_tasks = MagicMock()

        candidates = Candidates()

        request = RecommendationContext(
            user_id=131,
            unique_id="ffd4a3a5-cd11-4112-92d7-070082615e81",
            timestamp="2013-02-24T12:28:45.610Z",
            visit_id="1",
            page_id="1",
            panel_id=panel_id,
            candidates=candidates,
            search_data=SearchData(),
            engine_version=engine_version,
        )
        response = await recommendations(request, background_tasks)
        return response, number_of_resources

    def check_response(
        self, response: RecommendationSet, number_of_resources: int, panel_id: str
    ):
        assert len(response.recommendations) == number_of_resources
        assert len(response.explanations) == number_of_resources
        assert len(response.explanations_short) == number_of_resources
        assert response.panel_id == panel_id

    @patch("httpx.AsyncClient.post")
    async def test_datasets(self, mock_post):
        panel_id = "datasets"
        resource_type = "dataset"
        engine_version = "content_visit"
        response, number_of_resources = await self.get_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_publication(self, mock_post):
        panel_id = "publications"
        resource_type = "publication"
        engine_version = "content_visit"
        response, number_of_resources = await self.get_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_software(self, mock_post):
        panel_id = "software"
        resource_type = "software"
        engine_version = "content_visit"
        response, number_of_resources = await self.get_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_other_research_product(self, mock_post):
        panel_id = "other_research_product"
        resource_type = "other"
        engine_version = "content_visit"
        response, number_of_resources = await self.get_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_trainings(self, mock_post):
        panel_id = "trainings"
        resource_type = "training"
        engine_version = "content_visit"
        response, number_of_resources = await self.get_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_services(self, mock_post):
        panel_id = "services"
        resource_type = "service"
        engine_version = "NCF"
        response, number_of_resources = await self.get_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_datasources(self, mock_post):
        panel_id = "data-sources"
        resource_type = "datasource"
        engine_version = "NCF"
        response, number_of_resources = await self.get_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_all(self, mock_post):
        panel_id = "all"
        resource_type = "training"
        engine_version = "content_visit"

        with open("tests/data/resources_sample.json", "r") as f:
            resources_sample = json.load(f)
        resources = resources_sample[resource_type]
        number_of_resources = len(resources)
        explantaions = [
            resource_type + " similar to services previously ordered by you."
        ] * number_of_resources
        explanations_short = [
            "Previously you ordered a similar services."
        ] * number_of_resources
        return_json = {
            "panel_id": panel_id,
            "recommendations": resources,
            "explanations": explantaions,
            "explanations_short": explanations_short,
            "scores": [0] * number_of_resources,
            "engine_version": engine_version,
        }

        mock_post.return_value.json = MagicMock()
        mock_post.return_value.status_code = 200
        mock_post.return_value.json.return_value = return_json
        background_tasks = MagicMock()

        candidates = Candidates()

        request = RecommendationContext(
            user_id=131,
            unique_id="ffd4a3a5-cd11-4112-92d7-070082615e81",
            timestamp="2013-02-24T12:28:45.610Z",
            visit_id="1",
            page_id="1",
            panel_id=panel_id,
            candidates=candidates,
            search_data=SearchData(),
            engine_version=engine_version,
        )
        response = await recommendations(request, background_tasks)
        self.check_response(
            response, settings_default.NUMBER_OF_RECOMMENDATION, panel_id
        )
