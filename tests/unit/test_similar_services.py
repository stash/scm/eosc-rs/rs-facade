import json
import unittest
from unittest import mock

from facade_app.api.endpoints.similar_services import similar_services_recommendation
from facade_app.api.schemas.recommendation_set import RecommendationSet
from facade_app.api.schemas.similar_services_recommendation_parameters import (
    SimilarServicesRecommendationParameters,
)
from facade_app.exceptions import RemoteApiUnavailableError
from tests.unit.mock_response import (
    MockResponse,
    mocked_requests_post_error,
)


def add_task(self, write_notification, jms_message):
    jms_message = json.loads(jms_message)
    assert jms_message["panel_id"] == "similar_services"
    assert isinstance((jms_message["recommendations"]), list)
    assert isinstance((jms_message["context"]), dict)
    assert jms_message["recommender_systems"][0] == "Content-based engine"


def mocked_requests_post(*args, **kwargs):
    return MockResponse(
        {
            "panel_id": "similar_services",
            "recommendations": [12, 23],
            "explanations": ["explanation1", "explanation2"],
            "explanations_short": ["explanation_short1", "explanation_short2"],
            "score": [0.7, 0.6],
            "engine_version": "v1",
        },
        200,
    )


class TestSimilarServices(unittest.IsolatedAsyncioTestCase):
    def get_params(self) -> SimilarServicesRecommendationParameters:
        return SimilarServicesRecommendationParameters(
            user_id="1",
            unique_id="unique_id",
            aai_uid="aai_uid",
            timestamp="2013-02-24T12:28:45.610Z",
            service_id="1",
            num="1",
        )

    @mock.patch(
        "requests.post",
        side_effect=mocked_requests_post,
    )
    async def test_fetch(self, mock_post):
        background_tasks = mock.MagicMock()
        background_tasks.add_task = add_task.__get__(background_tasks)
        params = self.get_params()
        response = await similar_services_recommendation(params, background_tasks)
        self.assertEqual(
            response,
            RecommendationSet(
                panel_id="similar_services",
                recommendations=[12, 23],
                explanations=["explanation1", "explanation2"],
                explanations_short=["explanation_short1", "explanation_short2"],
                engine_versions=["v1"],
            ),
        )

    @mock.patch(
        "requests.post",
        side_effect=mocked_requests_post_error,
    )
    async def test_error(self, mock_post):
        background_tasks = mock.MagicMock()
        background_tasks.add_task = add_task.__get__(background_tasks)
        params = self.get_params()
        with self.assertRaises(RemoteApiUnavailableError):
            await similar_services_recommendation(params, background_tasks)
