import json
from typing import List
import unittest
from unittest.mock import Mock

from fastapi.testclient import TestClient
from pydantic import parse_obj_as
from requests.models import Response

from facade_app.api.endpoints import recommendations
from facade_app.engines.engine_recommendation import engine_recommendation
from facade_app.api.schemas.recommendation_context import RecommendationContext
from facade_app.api.schemas.recommendation_set_with_score import (
    RecommendationSetWithScore,
)
from facade_app.main import app
from facade_app.structures.types import PanelId


def mock_write_notification(jms_message):
    jms_message = json.loads(jms_message)
    assert jms_message["panel_id"] == "datasets"
    assert isinstance((jms_message["recommendations"]), list)
    assert isinstance((jms_message["context"]), dict)
    assert jms_message["recommender_systems"][0] == "Online_engine"


async def mock_get_recommendation(
    url: str,
    request: RecommendationContext,
    panel_id: PanelId,
    engine_version: str,
    service_name: str,
    data: dict[str, List[RecommendationSetWithScore]],
    status_data: dict[str],
    sources: List,
    recommender_systems: List,
):
    response = Mock(spec=Response)
    response.json.return_value = json.load(open("tests/example_response_body.json"))
    response.status_code = 200

    if response.status_code == 200:
        if service_name not in recommender_systems:
            recommender_systems.append(service_name)
        response_json = response.json()
        engine_version = (
            response_json["engine_version"] if "engine_version" in response_json else ""
        )
        sources.append(
            {
                "panel_id": panel_id,
                "recommender_system": service_name,
                "recommendations": response_json["recommendations"],
                "engine_version": engine_version,
            }
        )
        data[panel_id] = parse_obj_as(RecommendationSetWithScore, response_json)
        status_data[panel_id] = "ok"


class TestUnitRecommendations(unittest.TestCase):
    def test_unit_recommendation(self):
        engine_recommendation.get_recommendation = mock_get_recommendation
        recommendations.write_notification = mock_write_notification
        client = TestClient(app)

        data = json.load(open("tests/example_request_body.json"))
        response = client.post("/recommendations", data=json.dumps(data))
        response_json = response.json()

        assert response.status_code == 200
        assert response_json["panel_id"] == "datasets"
        assert isinstance((response_json["recommendations"]), list)
        assert isinstance((response_json["explanations"]), list)
        assert isinstance((response_json["explanations_short"]), list)
        assert response_json["engine_versions"][0] == "content_visit"
