import json
import unittest
from unittest import mock

import requests
from pydantic import parse_obj_as

from facade_app.api.endpoints.similar_resources import recommendations
from facade_app.api.schemas.similar_resources_context import SimilarResourcesContext
from facade_app.exceptions import RemoteApiUnavailableError
from tests.unit.mock_response import MockResponse


def add_task(self, write_notification, jms_message):
    jms_message = json.loads(jms_message)
    assert jms_message["panel_id"] == "datasets"
    assert isinstance((jms_message["recommendations"]), list)
    assert isinstance((jms_message["context"]), dict)
    assert jms_message["recommender_systems"][0] == "Online_engine"


def mocked_requests_post(*args, **kwargs):
    return MockResponse(
        {
            "panel_id": "datasets",
            "recommendations": ["1", "2", "3"],
            "explanations": ["1", "2", "3"],
            "explanations_short": ["1", "2", "3"],
            "scores": [3, 2, 1],
            "engine_version": "similar_resources",
        },
        200,
    )


def mocked_requests_exception_post(*args, **kwargs):
    return MockResponse(
        {"Error": "error"},
        400,
    )


class TestSimilarResources(unittest.IsolatedAsyncioTestCase):
    @mock.patch(
        "requests.post",
        side_effect=mocked_requests_post,
    )
    async def test_recommendation(self, mock_post):
        background_tasks = mock.MagicMock()
        background_tasks.add_task = add_task.__get__(background_tasks)
        params = parse_obj_as(
            SimilarResourcesContext,
            {
                "aai_uid": "0000-0001-5135-5758@orcid.orgs",
                "unique_id": "00453992-c871-2ed5-3535-b4748103bc97",
                "timestamp": "2013-02-24T12:28:45.610Z",
                "page_id": "1",
                "client_id": "marketplace",
                "panel_id": "datasets",
                "resource_type": "datasets",
                "resource_id": "50|doi_dedup___::16bef977316203eba09dfacea6b481d6",
            },
        )
        response = await recommendations(params, background_tasks)
        self.assertEqual(response.panel_id, "datasets")
        self.assertEqual(response.recommendations, ["1", "2", "3"])
        self.assertEqual(response.explanations, ["1", "2", "3"])
        self.assertEqual(response.explanations_short, ["1", "2", "3"])

    @mock.patch(
        "requests.post",
        side_effect=mocked_requests_exception_post,
    )
    async def test_exception_400(self, mock_post):
        background_tasks = mock.MagicMock()
        background_tasks.add_task = add_task.__get__(background_tasks)
        params = parse_obj_as(
            SimilarResourcesContext,
            {
                "aai_uid": "0000-0001-5135-5758@orcid.orgs",
                "unique_id": "00453992-c871-2ed5-3535-b4748103bc97",
                "timestamp": "2013-02-24T12:28:45.610Z",
                "page_id": "1",
                "client_id": "marketplace",
                "panel_id": "datasets",
                "resource_type": "datasets",
                "resource_id": "50|doi_dedup___::16bef977316203eba09dfacea6b481d6",
            },
        )
        with self.assertRaises(RemoteApiUnavailableError):
            await recommendations(params, background_tasks)

    @mock.patch(
        "requests.post",
        side_effect=requests.exceptions.ConnectionError,
    )
    async def test_exception_connection_error(self, mock_post):
        background_tasks = mock.MagicMock()
        background_tasks.add_task = add_task.__get__(background_tasks)
        params = parse_obj_as(
            SimilarResourcesContext,
            {
                "aai_uid": "0000-0001-5135-5758@orcid.orgs",
                "unique_id": "00453992-c871-2ed5-3535-b4748103bc97",
                "timestamp": "2013-02-24T12:28:45.610Z",
                "page_id": "1",
                "client_id": "marketplace",
                "panel_id": "datasets",
                "resource_type": "datasets",
                "resource_id": "50|doi_dedup___::16bef977316203eba09dfacea6b481d6",
            },
        )
        with self.assertRaises(RemoteApiUnavailableError):
            await recommendations(params, background_tasks)
