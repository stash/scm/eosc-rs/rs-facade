import json
from typing import Tuple
from unittest.mock import MagicMock

from asynctest import patch
import httpx

from facade_app.api.endpoints.recommendations import recommendations
from facade_app.api.schemas.recommendation_context import (
    Candidates,
    RecommendationContext,
    SearchData,
)
from facade_app.api.schemas.recommendation_set import RecommendationSet
from facade_app.exceptions import EmptyCandidateList, RemoteApiUnavailableError
from tests.base_test_api_helper import BaseTestApi


class TestSortCandidatesMock(BaseTestApi):
    def get_empty_recommendation_context(self, panel_id: str, engine_version: str):
        return RecommendationContext(
            user_id=131,
            unique_id="ffd4a3a5-cd11-4112-92d7-070082615e81",
            timestamp="2013-02-24T12:28:45.610Z",
            visit_id="1",
            page_id="1",
            panel_id=panel_id,
            candidates={},
            search_data=SearchData(),
            engine_version=engine_version,
        )

    async def get_sort_resource(
        self, mock_post, panel_id: str, resource_type: str, engine_version: str
    ) -> Tuple[RecommendationSet, int]:
        return_json, number_of_resources, resources = self.prepare_response(
            panel_id, resource_type, engine_version
        )

        mock_post.return_value.json = MagicMock()
        mock_post.return_value.status_code = 200
        mock_post.return_value.json.return_value = return_json
        background_tasks = MagicMock()

        candidates = Candidates(
            dataset=[],
            publication=[],
            software=[],
            other=[],
            training=[],
            service=[],
            datasource=[],
        )
        candidates.__dict__[resource_type] = resources

        request = self.get_empty_recommendation_context(panel_id, engine_version)
        request.candidates = candidates
        response = await recommendations(request, background_tasks)
        return response, number_of_resources

    def check_response(
        self, response: RecommendationSet, number_of_resources: int, panel_id: str
    ):
        assert len(response.recommendations) == number_of_resources
        assert len(response.explanations) == number_of_resources
        assert len(response.explanations_short) == number_of_resources
        assert response.panel_id == panel_id

    @patch("httpx.AsyncClient.post")
    async def test_sort_datasets(self, mock_post):
        panel_id = "datasets"
        resource_type = "dataset"
        engine_version = "content_visit_sort"
        response, number_of_resources = await self.get_sort_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_sort_publication(self, mock_post):
        panel_id = "publications"
        resource_type = "publication"
        engine_version = "content_visit_sort"
        response, number_of_resources = await self.get_sort_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_sort_software(self, mock_post):
        panel_id = "software"
        resource_type = "software"
        engine_version = "content_visit_sort"
        response, number_of_resources = await self.get_sort_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_sort_other_research_product(self, mock_post):
        panel_id = "other_research_product"
        resource_type = "other"
        engine_version = "content_visit_sort"
        response, number_of_resources = await self.get_sort_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_sort_trainings(self, mock_post):
        panel_id = "trainings"
        resource_type = "training"
        engine_version = "content_visit_sort"
        response, number_of_resources = await self.get_sort_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_sort_services(self, mock_post):
        panel_id = "services"
        resource_type = "service"
        engine_version = "NCFRanking"
        response, number_of_resources = await self.get_sort_resource(
            mock_post, panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_sort_all(self, mock_post):
        panel_id = "all"
        resource_type = "training"
        engine_version = "content_visit_sort"

        with open("tests/data/resources_sample.json", "r") as f:
            resources_sample = json.load(f)
        resources = resources_sample[resource_type]
        number_of_resources = len(resources)
        explantaions = [
            resource_type + " similar to services previously ordered by you."
        ] * number_of_resources
        explanations_short = [
            "Previously you ordered a similar services."
        ] * number_of_resources
        return_json = {
            "panel_id": panel_id,
            "recommendations": resources,
            "explanations": explantaions,
            "explanations_short": explanations_short,
            "scores": [0] * number_of_resources,
            "engine_version": engine_version,
        }

        mock_post.return_value.json = MagicMock()
        mock_post.return_value.status_code = 200
        mock_post.return_value.json.return_value = return_json
        background_tasks = MagicMock()

        candidates = Candidates(
            dataset=resources_sample["dataset"],
            publication=resources_sample["publication"],
            software=resources_sample["software"],
            other=resources_sample["other"],
            training=resources_sample["training"],
            service=resources_sample["service"],
            datasource=resources_sample["datasource"],
        )

        request = self.get_empty_recommendation_context(panel_id, engine_version)
        request.candidates = candidates
        response = await recommendations(request, background_tasks)
        self.check_response(response, number_of_resources * 7, panel_id)

    @patch("httpx.AsyncClient.post")
    async def test_sort_empty_candidates(self, mock_post):
        panel_id = "datasets"
        engine_version = "content_visit_sort"
        request = self.get_empty_recommendation_context(panel_id, engine_version)
        background_tasks = MagicMock()
        with self.assertRaises(EmptyCandidateList):
            await recommendations(request, background_tasks)

    @patch("httpx.AsyncClient.post", side_effect=httpx.TimeoutException("Timeout"))
    async def test_sort_timeout(self, mock_post):
        panel_id = "datasets"
        engine_version = "content_visit_sort"
        request = self.get_empty_recommendation_context(panel_id, engine_version)
        request.candidates = Candidates(dataset=["1"])
        background_tasks = MagicMock()
        with self.assertRaises(RemoteApiUnavailableError):
            await recommendations(request, background_tasks)

    @patch("httpx.AsyncClient.post", side_effect=httpx.ConnectError("ConnectError"))
    async def test_sort_connect_error(self, mock_post):
        panel_id = "datasets"
        engine_version = "content_visit_sort"
        request = self.get_empty_recommendation_context(panel_id, engine_version)
        request.candidates = Candidates(dataset=["1"])
        background_tasks = MagicMock()
        with self.assertRaises(RemoteApiUnavailableError):
            await recommendations(request, background_tasks)
