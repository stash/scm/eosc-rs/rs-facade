import json
import unittest
from unittest import mock

from facade_app.api.endpoints.project_completion import (
    project_completion_recommendation,
)
from facade_app.api.schemas.project_completion_recommendation import (
    ProjectCompletionRecommendation,
)
from facade_app.api.schemas.project_completion_recommendation_parameters import (
    ProjectCompletionRecommendationParameters,
)
from facade_app.exceptions import RemoteApiUnavailableError
from tests.unit.mock_response import mocked_requests_post, mocked_requests_post_error


def add_task(self, write_notification, jms_message):
    jms_message = json.loads(jms_message)
    assert jms_message["panel_id"] == "Auto-completion"
    assert isinstance((jms_message["recommendations"]), list)
    assert isinstance((jms_message["context"]), dict)
    assert jms_message["recommender_systems"][0] == "Auto-completion"


class TestProjectCompletion(unittest.IsolatedAsyncioTestCase):
    @mock.patch(
        "requests.post",
        side_effect=mocked_requests_post,
    )
    async def test_fetch(self, mock_post):
        background_tasks = mock.MagicMock()
        background_tasks.add_task = add_task.__get__(background_tasks)

        params = ProjectCompletionRecommendationParameters(project_id="1", num=1)
        response = await project_completion_recommendation(params, background_tasks)
        self.assertEqual(
            response, [ProjectCompletionRecommendation(service_id=1, score=2.0)]
        )

    @mock.patch(
        "requests.post",
        side_effect=mocked_requests_post_error,
    )
    async def test_error(self, mock_post):
        background_tasks = mock.MagicMock()
        background_tasks.add_task = add_task.__get__(background_tasks)
        params = ProjectCompletionRecommendationParameters(project_id="1", num=1)
        with self.assertRaises(RemoteApiUnavailableError):
            await project_completion_recommendation(params, background_tasks)
