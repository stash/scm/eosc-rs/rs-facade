import json


class MockHeaders:
    def get(self, field: str) -> str:
        return "application/json"


class MockResponse:
    def __init__(self, json_data, status_code, url=""):
        self.json_data = json_data
        self.status_code = status_code
        self.text = json.dumps(json_data)
        self.url = url
        self.headers = MockHeaders()

    def json(self):
        return self.json_data


def mocked_requests_post(*args, **kwargs):
    return MockResponse([{"service_id": 1, "score": 2.0}], 200)


def mocked_requests_post_error(*args, **kwargs):
    return MockResponse([{"message": "error"}], 400)
