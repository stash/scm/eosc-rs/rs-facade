from typing import Tuple
from unittest.mock import MagicMock

from facade_app.api.endpoints.recommendations import recommendations
from facade_app.api.schemas.recommendation_context import (
    Candidates,
    RecommendationContext,
    SearchData,
)
from facade_app.api.schemas.recommendation_set import RecommendationSet
from facade_app.settings import settings_default
from tests.base_test_api_helper import BaseTestApi


class TestRecomendationIntegration(BaseTestApi):
    async def get_resource(
        self, panel_id: str, resource_type: str, engine_version: str
    ) -> Tuple[RecommendationSet, int]:
        background_tasks = MagicMock()
        candidates = Candidates()

        request = RecommendationContext(
            user_id=131,
            unique_id="ffd4a3a5-cd11-4112-92d7-070082615e81",
            timestamp="2013-02-24T12:28:45.610Z",
            visit_id="1",
            page_id="1",
            panel_id=panel_id,
            candidates=candidates,
            search_data=SearchData(),
            engine_version=engine_version,
        )
        response = await recommendations(request, background_tasks)
        return response, 3

    def check_response(
        self, response: RecommendationSet, number_of_resources: int, panel_id: str
    ):
        assert len(response.recommendations) == number_of_resources
        assert len(response.explanations) == number_of_resources
        assert len(response.explanations_short) == number_of_resources
        assert response.panel_id == panel_id

    async def test_datasets(self):
        panel_id = "datasets"
        resource_type = "dataset"
        engine_version = "content_visit"
        response, number_of_resources = await self.get_resource(
            panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    async def test_publication(self):
        panel_id = "publications"
        resource_type = "publication"
        engine_version = "content_visit"
        response, number_of_resources = await self.get_resource(
            panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    async def test_software(self):
        panel_id = "software"
        resource_type = "software"
        engine_version = "content_visit"
        response, number_of_resources = await self.get_resource(
            panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    async def test_other_research_product(self):
        panel_id = "other_research_product"
        resource_type = "other"
        engine_version = "content_visit"
        response, number_of_resources = await self.get_resource(
            panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    async def test_trainings(self):
        panel_id = "trainings"
        resource_type = "training"
        engine_version = "content_visit"
        response, number_of_resources = await self.get_resource(
            panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    async def test_all(self):
        panel_id = "all"
        engine_version = "content_visit"

        background_tasks = MagicMock()

        candidates = Candidates()

        request = RecommendationContext(
            user_id=131,
            unique_id="ffd4a3a5-cd11-4112-92d7-070082615e81",
            timestamp="2013-02-24T12:28:45.610Z",
            visit_id="1",
            page_id="1",
            panel_id=panel_id,
            candidates=candidates,
            search_data=SearchData(),
            engine_version=engine_version,
        )
        response = await recommendations(request, background_tasks)
        self.check_response(
            response, settings_default.NUMBER_OF_RECOMMENDATION, panel_id
        )
