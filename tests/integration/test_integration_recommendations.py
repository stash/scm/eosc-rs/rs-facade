import json
import logging
import time
import unittest

import pytest
import stomp
from fastapi.testclient import TestClient

from facade_app.main import app
from facade_app.settings import settings_default

logger = logging.getLogger(__name__)

client = TestClient(app)


def connect_and_subscribe(conn):
    conn.connect(settings_default.JMS_USER, settings_default.JMS_PASSWORD, wait=True)
    conn.subscribe(
        destination=settings_default.JMS_RECOMMENDATION_TOPIC, id=1, ack="auto"
    )


class JMSListener(stomp.ConnectionListener):
    def __init__(self, conn):
        self.conn = conn

    def on_error(self, frame):
        logger.error('Received an error "%s"' % frame.body)

    def on_message(self, frame):
        logger.info("Received a message: " + frame.body)

        assert len(str(frame.body)) > 300

        for x in range(10):
            logger.info(x)
            time.sleep(1)
        logger.info("Processed message")

    def on_disconnected(self):
        logger.info("Disconnected")
        connect_and_subscribe(self.conn)


class TestUnitRecommendations(unittest.TestCase):
    def test_integration_recommendations_endpoint(self):
        data = json.load(open("./tests/example_request_body.json"))
        response = client.post("/recommendations", data=json.dumps(data))
        response_json = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_json["panel_id"], "datasets")
        self.assertIsInstance(response_json["recommendations"], list)
        self.assertIsInstance(response_json["explanations"], list)
        self.assertIsInstance(response_json["explanations_short"], list)
        self.assertIn(
            response_json["engine_versions"][0],
            ["content_visit", "popularity_visit_total"],
        )

    @pytest.mark.filterwarnings("error::pytest.PytestUnhandledThreadExceptionWarning")
    def test_integration_jms(self):
        jms_connector = stomp.Connection(
            [(settings_default.JMS_URL, settings_default.JMS_PORT)],
            heartbeats=(4000, 4000),
        )
        jms_connector.set_listener(
            settings_default.JMS_RECOMMENDATION_TOPIC, JMSListener(jms_connector)
        )
        if settings_default.SSL:
            jms_connector.set_ssl(
                [(settings_default.JMS_URL, settings_default.JMS_PORT)]
            )

        connect_and_subscribe(jms_connector)

        data = json.load(open("./tests/example_request_body.json"))
        response = client.post("/recommendations", data=json.dumps(data))
        self.assertEqual(response.status_code, 200)

        time.sleep(60)
        jms_connector.disconnect()
