from typing import Tuple
from unittest.mock import MagicMock

from facade_app.api.endpoints.recommendations import recommendations
from facade_app.api.schemas.recommendation_context import (
    Candidates,
    RecommendationContext,
    SearchData,
)
from facade_app.api.schemas.recommendation_set import RecommendationSet
from tests.base_test_api_helper import BaseTestApi


class TestSortCandidatesIntegration(BaseTestApi):
    async def get_sort_resource(
        self, panel_id: str, resource_type: str, engine_version: str
    ) -> Tuple[RecommendationSet, int]:
        _, number_of_resources, resources = self.prepare_response(
            panel_id, resource_type, engine_version
        )

        background_tasks = MagicMock()

        candidates = Candidates(
            dataset=[],
            publication=[],
            software=[],
            other=[],
            training=[],
            service=[],
            datasource=[],
        )
        candidates.__dict__[resource_type] = resources

        request = RecommendationContext(
            user_id=131,
            unique_id="ffd4a3a5-cd11-4112-92d7-070082615e81",
            timestamp="2013-02-24T12:28:45.610Z",
            visit_id="1",
            page_id="1",
            panel_id=panel_id,
            candidates=candidates,
            search_data=SearchData(),
            engine_version=engine_version,
        )
        response = await recommendations(request, background_tasks)
        return response, number_of_resources

    def check_response(
        self, response: RecommendationSet, number_of_resources: int, panel_id: str
    ):
        self.assertEqual(len(response.recommendations), number_of_resources)
        self.assertEqual(len(response.explanations), number_of_resources)
        self.assertEqual(len(response.explanations_short), number_of_resources)
        self.assertEqual(response.panel_id, panel_id)

    async def test_sort_datasets(self):
        panel_id = "datasets"
        resource_type = "dataset"
        engine_version = "content_visit_sort"
        response, number_of_resources = await self.get_sort_resource(
            panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    async def test_sort_publication(self):
        panel_id = "publications"
        resource_type = "publication"
        engine_version = "content_visit_sort"
        response, number_of_resources = await self.get_sort_resource(
            panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    async def test_sort_software(self):
        panel_id = "software"
        resource_type = "software"
        engine_version = "content_visit_sort"
        response, number_of_resources = await self.get_sort_resource(
            panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    async def test_sort_other_research_product(self):
        panel_id = "other_research_product"
        resource_type = "other"
        engine_version = "content_visit_sort"
        response, number_of_resources = await self.get_sort_resource(
            panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)

    async def test_sort_trainings(self):
        panel_id = "trainings"
        resource_type = "training"
        engine_version = "content_visit_sort"
        response, number_of_resources = await self.get_sort_resource(
            panel_id, resource_type, engine_version
        )
        self.check_response(response, number_of_resources, panel_id)
