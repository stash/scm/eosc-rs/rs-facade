import json
import unittest
from typing import Dict, List, Tuple

from facade_app.api_client import api_client


class BaseTestApi(unittest.IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        await api_client.create_session()

    async def asyncTearDown(self):
        await api_client.close_session()

    def prepare_response(
        self, panel_id: str, resource_type: str, engine_version: str
    ) -> Tuple[Dict, int, List[str]]:
        with open("tests/data/resources_sample.json", "r") as f:
            resources_sample = json.load(f)
        resources = resources_sample[resource_type]
        number_of_resources = len(resources)
        explanations = [
            resource_type + " similar to services previously ordered by you."
        ] * number_of_resources
        explanations_short = [
            "Previously you ordered a similar services."
        ] * number_of_resources
        return_json = {
            "panel_id": panel_id,
            "recommendations": resources,
            "explanations": explanations,
            "explanations_short": explanations_short,
            "scores": [0] * number_of_resources,
            "engine_version": engine_version,
        }
        return return_json, number_of_resources, resources
