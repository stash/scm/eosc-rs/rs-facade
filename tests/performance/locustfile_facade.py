from locust import HttpUser, task
import pandas as pd
import random

recommendation_context = {
    "unique_id": "00453992-c871-2ed5-3535-b4748103bc97",
    "timestamp": "2013-02-24T12:28:45.610Z",
    "visit_id": "1",
    "page_id": "1",
    "client_id": "marketplace",
    "panel_id": "datasets",
    "candidates": [],
    "search_data": {},
    "engine_version": "content_visit",
}
resource_types = [
    "datasets",
    "publications",
    "software",
    "trainings",
    "other_research_product",
]
users_aai_ids = pd.read_csv("users_aai_ids.csv", index_col=0)


class RecommendationsAAIUser(HttpUser):
    user_id = users_aai_ids.sample(1)["id"].item()

    @task
    def get_recommendations_aai(self):
        request = recommendation_context
        request["aai_uid"] = self.user_id
        resource_type = random.choice(resource_types)
        request["panel_id"] = resource_type
        self.client.post(
            "/recommendations",
            json=request,
            name=f"/recommendations {resource_type} aai user",
        )


class RecommendationsAnonymusUser(HttpUser):
    @task
    def get_recommendations_anonymus(self):
        request = recommendation_context
        resource_type = random.choice(resource_types)
        request["panel_id"] = resource_type
        self.client.post(
            "/recommendations",
            json=request,
            name=f"/recommendations {resource_type} anonymus user",
        )
