from locust import HttpUser, task
import pandas as pd
import random

recommendation_context = {
    "unique_id": "00453992-c871-2ed5-3535-b4748103bc97",
    "timestamp": "2013-02-24T12:28:45.610Z",
    "visit_id": "1",
    "page_id": "1",
    "client_id": "marketplace",
    "panel_id": "datasets",
    "candidates": {},
    "search_data": {},
    "engine_version": "content_visit_sort",
}
resource_types = [
    "publications",
    "software",
    "trainings",
    "otherresearchproduct",
    "datasets",
]
resource_type_mapping = {
    "publications": "publication",
    "software": "software",
    "trainings": "training",
    "other_research_product": "other",
    "datasets": "dataset",
}
resources_ids = pd.read_parquet("resources_id.parquet")


class RecommendationsAAIUser(HttpUser):
    @task
    def get_recommendations_aai(self):
        request = recommendation_context
        resource_type = random.choice(resource_types)
        request["aai_uid"] = "performance_test"
        request["panel_id"] = (
            resource_type
            if resource_type != "otherresearchproduct"
            else "other_research_product"
        )
        request["candidates"] = {
            resource_type_mapping[request["panel_id"]]: resources_ids[
                resources_ids["type"] == resource_type
            ]
            .sample(50)["id"]
            .to_list()
        }
        self.client.post(
            "/recommendations",
            json=request,
            name=f"/recommendations {resource_type} 50",
        )
